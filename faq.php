<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html"); ?>
<!-- /Top Head -->
<script>
  //<![CDATA[
  $(document).ready(function(){
      $('#navigation>ul>li>a').removeClass('selected');
	  $('#navigation>ul>li:nth-child(4)>a').addClass('selected');
  });
  //]]>
</script>
<body class="page-detail">
<!-- header -->
<?php include("incs/header.html"); ?>
<!-- /header -->
<div id="toc" class="warpper">
	
	
	<section id="home" class="sec-faq">
		<div class="container">
			
			<div class="top-zone">
				<ul class="idTabs">
					<li><a href="#q1" title="xxx">link1</a></li>
				</ul>
				<div class="contentTabs">
					<div class="faq-bx" id="q1">
						<div class="accordion">
							<a class="acc-h" href="javascript:;" title="question 1">question 1 <i class="fas fa-info-circle"></i></a>
							<div class="pane">
								<ol class="description">
									<li>ราคาไม่รวม VAT 7%</li>
									<li class="text-wrap">*ราคาสำหรับพื้นที่จัดส่งในเขตกรุงเทพฯและปริมณฑลเท่านั้น สำหรับพื้นที่จัดส่งพัทยา <a href="http://sprinkle-th.com/contact.php" >กรุณาติดต่อตัวแทนจำหน่าย</a></li>
								</ol>
							</div>

							<a class="acc-h" href="javascript:;" title="question 2">question 2<i class="fas fa-info-circle"></i></a>
							<div class="pane">
								<ol class="description">
									<li>ราคาไม่รวม VAT 7%</li>
									<li class="text-wrap">*ราคาสำหรับพื้นที่จัดส่งในเขตกรุงเทพฯและปริมณฑลเท่านั้น สำหรับพื้นที่จัดส่งพัทยา <a href="http://sprinkle-th.com/contact.php" >กรุณาติดต่อตัวแทนจำหน่าย</a></li>
								</ol>
							</div>

							<a class="acc-h" href="javascript:;" title="question 3">question 3 <i class="fas fa-info-circle"></i></a>
							<div class="pane">
								<ol class="description">
									<li>ราคาไม่รวม VAT 7%</li>
									<li class="text-wrap">*ราคาสำหรับพื้นที่จัดส่งในเขตกรุงเทพฯและปริมณฑลเท่านั้น สำหรับพื้นที่จัดส่งพัทยา <a href="http://sprinkle-th.com/contact.php" >กรุณาติดต่อตัวแทนจำหน่าย</a></li>
								</ol>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</section>
	
		
</div>

<?php include("incs/footer.html"); ?>

<!-- Java Script -->
<?php include("incs/js.html"); ?>
<!-- /JS -->
</body>
</html>
