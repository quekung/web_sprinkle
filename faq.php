<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html"); ?>
<!-- /Top Head -->
<script>
  //<![CDATA[
  $(document).ready(function(){
      $('#navigation>ul>li>a').removeClass('selected');
	  $('#navigation>ul>li:nth-child(4)>a').addClass('selected');
  });
  //]]>
</script>
<body class="page-detail">
<!-- header -->
<?php include("incs/header.html"); ?>
<!-- /header -->
<div id="toc" class="wrapper">
	
	
	<section id="home" class="sec-faq">
		<div class="container">
			<h2 class="h-topic txt-c mb15-xs mb30-sm">Frequently asked questions</h2>
			<div class="top-zone faq-mob">
				<ul class="idTabs">
					<li class="active"><a class="selected" href="#q1" title="Quality and production"><i class="icon"><img src="assets/imgs/faq-quality.png"></i> <span>Quality and production</span></a></li>
					<li><a href="#q2" title="Becoming a member"><i class="icon"><img src="assets/imgs/faq-becoming.png"></i> <span>Becoming a member</span></a></li>
					<li><a href="#q3" title="Delivery service"><i class="icon"><img src="assets/imgs/faq-delivery.png"></i> <span>Delivery service</span></a></li>
					<li><a href="#q4" title="Deposit bottle returning"><i class="icon"><img src="assets/imgs/faq-deposit.png"></i> <span>Deposit bottle returning</span></a></li>
					<li><a href="#q5" title="Contact information"><i class="icon"><img src="assets/imgs/faq-contact-info.png"></i> <span>Contact information</span></a></li>
				</ul>
				<div class="contentTabs">
					<!-- q1 -->
					<div class="faq-bx" id="q1">
						<h3 class="h-topic txt-c mb15-xs mb30-sm">Quality and production</h3>
						<div class="accordion">
							<div class="bx expand">
								<a class="acc-h active" href="javascript:;" title="question 1">How can I discontinue my Sprinkle service and receive my deposit? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane active" style="display: block;">
									<p>You can contact us and provide the number of deposit bottles you wish to get a refund for, your bank account number (the account holder’s name should match the name in the membership information), so we can transfer the refund to you. We will notify you of the date when our staff will collect the bottles and process to transfer you the refund within 18 working days from the day we receive the returned bottles.</p>
								</div>
							</div>
							
							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 2">Can I return some of the bottles but stay a member? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<ol class="description">
										<li>ราคาไม่รวม VAT 7%</li>
										<li class="text-wrap">*ราคาสำหรับพื้นที่จัดส่งในเขตกรุงเทพฯและปริมณฑลเท่านั้น สำหรับพื้นที่จัดส่งพัทยา <a href="http://sprinkle-th.com/contact.php" >กรุณาติดต่อตัวแทนจำหน่าย</a></li>
									</ol>
								</div>
							</div>

							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 3">Is it necessary to wait for the delivery staff to collect the bottles? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<p>You can contact us and provide the number of deposit bottles you wish to get a refund for, your bank account number (the account holder’s name should match the name in the membership information), so we can transfer the refund to you. We will notify you of the date when our staff will collect the bottles and process to transfer you the refund within 18 working days from the day we receive the returned bottles.</p>
								</div>
							</div>
							
							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 4">I want to discontinue service, but I have some coupons left. Can I refund these coupons? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<ol class="description">
										<li>ราคาไม่รวม VAT 7%</li>
										<li class="text-wrap">*ราคาสำหรับพื้นที่จัดส่งในเขตกรุงเทพฯและปริมณฑลเท่านั้น สำหรับพื้นที่จัดส่งพัทยา <a href="http://sprinkle-th.com/contact.php" >กรุณาติดต่อตัวแทนจำหน่าย</a></li>
									</ol>
								</div>	
							</div>
							
							<!-- more 4 hidden -->
							
							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 5">How can I discontinue my Sprinkle service and receive my deposit? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<p>You can contact us and provide the number of deposit bottles you wish to get a refund for, your bank account number (the account holder’s name should match the name in the membership information), so we can transfer the refund to you. We will notify you of the date when our staff will collect the bottles and process to transfer you the refund within 18 working days from the day we receive the returned bottles.</p>
								</div>
							</div>
							
							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 6">Can I return some of the bottles but stay a member? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<ol class="description">
										<li>ราคาไม่รวม VAT 7%</li>
										<li class="text-wrap">*ราคาสำหรับพื้นที่จัดส่งในเขตกรุงเทพฯและปริมณฑลเท่านั้น สำหรับพื้นที่จัดส่งพัทยา <a href="http://sprinkle-th.com/contact.php" >กรุณาติดต่อตัวแทนจำหน่าย</a></li>
									</ol>
								</div>
							</div>
							
							<div class="ctrl-btn mt20-xs">
								<a class="sw-showmore" href="javascript:;" title="show more">show more</a>
							</div>
						</div>
					</div>
					<!-- q2 -->
					<div class="faq-bx" id="q2">
						<h3 class="h-topic txt-c mb15-xs mb30-sm">Becoming a member</h3>
						<div class="accordion">
							<div class="bx expand">
								<a class="acc-h active" href="javascript:;" title="question 1">2.1 How can I discontinue my Sprinkle service and receive my deposit? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane active" style="display: block;">
									<p>You can contact us and provide the number of deposit bottles you wish to get a refund for, your bank account number (the account holder’s name should match the name in the membership information), so we can transfer the refund to you. We will notify you of the date when our staff will collect the bottles and process to transfer you the refund within 18 working days from the day we receive the returned bottles.</p>
								</div>
							</div>
							
							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 2">2.2 Can I return some of the bottles but stay a member? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<ol class="description">
										<li>ราคาไม่รวม VAT 7%</li>
										<li class="text-wrap">*ราคาสำหรับพื้นที่จัดส่งในเขตกรุงเทพฯและปริมณฑลเท่านั้น สำหรับพื้นที่จัดส่งพัทยา <a href="http://sprinkle-th.com/contact.php" >กรุณาติดต่อตัวแทนจำหน่าย</a></li>
									</ol>
								</div>
							</div>

							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 3">2.3 Is it necessary to wait for the delivery staff to collect the bottles? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<p>You can contact us and provide the number of deposit bottles you wish to get a refund for, your bank account number (the account holder’s name should match the name in the membership information), so we can transfer the refund to you. We will notify you of the date when our staff will collect the bottles and process to transfer you the refund within 18 working days from the day we receive the returned bottles.</p>
								</div>
							</div>
							
							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 4">2.4 I want to discontinue service, but I have some coupons left. Can I refund these coupons? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<ol class="description">
										<li>ราคาไม่รวม VAT 7%</li>
										<li class="text-wrap">*ราคาสำหรับพื้นที่จัดส่งในเขตกรุงเทพฯและปริมณฑลเท่านั้น สำหรับพื้นที่จัดส่งพัทยา <a href="http://sprinkle-th.com/contact.php" >กรุณาติดต่อตัวแทนจำหน่าย</a></li>
									</ol>
								</div>	
							</div>
							
							<!-- more 4 hidden -->
							
							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 5">2.5 How can I discontinue my Sprinkle service and receive my deposit? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<p>You can contact us and provide the number of deposit bottles you wish to get a refund for, your bank account number (the account holder’s name should match the name in the membership information), so we can transfer the refund to you. We will notify you of the date when our staff will collect the bottles and process to transfer you the refund within 18 working days from the day we receive the returned bottles.</p>
								</div>
							</div>
							
							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 6">2.6 Can I return some of the bottles but stay a member? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<ol class="description">
										<li>ราคาไม่รวม VAT 7%</li>
										<li class="text-wrap">*ราคาสำหรับพื้นที่จัดส่งในเขตกรุงเทพฯและปริมณฑลเท่านั้น สำหรับพื้นที่จัดส่งพัทยา <a href="http://sprinkle-th.com/contact.php" >กรุณาติดต่อตัวแทนจำหน่าย</a></li>
									</ol>
								</div>
							</div>
							
							<div class="ctrl-btn mt20-xs">
								<a class="sw-showmore" href="javascript:;" title="show more">show more</a>
							</div>
						</div>
					</div>
					<!-- q3 -->
					<div class="faq-bx" id="q3">
						<h3 class="h-topic txt-c mb15-xs mb30-sm">Delivery service</h3>
						<div class="accordion">
							<div class="bx expand">
								<a class="acc-h active" href="javascript:;" title="question 1">3.1 How can I discontinue my Sprinkle service and receive my deposit? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane active" style="display: block;">
									<p>You can contact us and provide the number of deposit bottles you wish to get a refund for, your bank account number (the account holder’s name should match the name in the membership information), so we can transfer the refund to you. We will notify you of the date when our staff will collect the bottles and process to transfer you the refund within 18 working days from the day we receive the returned bottles.</p>
								</div>
							</div>
							
							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 2">3.2 Can I return some of the bottles but stay a member? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<ol class="description">
										<li>ราคาไม่รวม VAT 7%</li>
										<li class="text-wrap">*ราคาสำหรับพื้นที่จัดส่งในเขตกรุงเทพฯและปริมณฑลเท่านั้น สำหรับพื้นที่จัดส่งพัทยา <a href="http://sprinkle-th.com/contact.php" >กรุณาติดต่อตัวแทนจำหน่าย</a></li>
									</ol>
								</div>
							</div>

							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 3">3.3 Is it necessary to wait for the delivery staff to collect the bottles? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<p>You can contact us and provide the number of deposit bottles you wish to get a refund for, your bank account number (the account holder’s name should match the name in the membership information), so we can transfer the refund to you. We will notify you of the date when our staff will collect the bottles and process to transfer you the refund within 18 working days from the day we receive the returned bottles.</p>
								</div>
							</div>
							
							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 4">3.4 I want to discontinue service, but I have some coupons left. Can I refund these coupons? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<ol class="description">
										<li>ราคาไม่รวม VAT 7%</li>
										<li class="text-wrap">*ราคาสำหรับพื้นที่จัดส่งในเขตกรุงเทพฯและปริมณฑลเท่านั้น สำหรับพื้นที่จัดส่งพัทยา <a href="http://sprinkle-th.com/contact.php" >กรุณาติดต่อตัวแทนจำหน่าย</a></li>
									</ol>
								</div>	
							</div>
							
							<!-- more 4 hidden -->
							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 6">3.5 Can I return some of the bottles but stay a member? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<ol class="description">
										<li>ราคาไม่รวม VAT 7%</li>
										<li class="text-wrap">*ราคาสำหรับพื้นที่จัดส่งในเขตกรุงเทพฯและปริมณฑลเท่านั้น สำหรับพื้นที่จัดส่งพัทยา <a href="http://sprinkle-th.com/contact.php" >กรุณาติดต่อตัวแทนจำหน่าย</a></li>
									</ol>
								</div>
							</div>
							
							<div class="ctrl-btn mt20-xs">
								<a class="sw-showmore" href="javascript:;" title="show more">show more</a>
							</div>
						</div>
					</div>
					<!-- q4 -->
					<div class="faq-bx" id="q4">
						<h3 class="h-topic txt-c mb15-xs mb30-sm">Deposit bottle returning</h3>
						<div class="accordion">
							<div class="bx expand">
								<a class="acc-h active" href="javascript:;" title="question 1">4.1 How can I discontinue my Sprinkle service and receive my deposit? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane active" style="display: block;">
									<p>You can contact us and provide the number of deposit bottles you wish to get a refund for, your bank account number (the account holder’s name should match the name in the membership information), so we can transfer the refund to you. We will notify you of the date when our staff will collect the bottles and process to transfer you the refund within 18 working days from the day we receive the returned bottles.</p>
								</div>
							</div>
							
							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 2">4.2 Can I return some of the bottles but stay a member? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<ol class="description">
										<li>ราคาไม่รวม VAT 7%</li>
										<li class="text-wrap">*ราคาสำหรับพื้นที่จัดส่งในเขตกรุงเทพฯและปริมณฑลเท่านั้น สำหรับพื้นที่จัดส่งพัทยา <a href="http://sprinkle-th.com/contact.php" >กรุณาติดต่อตัวแทนจำหน่าย</a></li>
									</ol>
								</div>
							</div>

							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 3">4.3 Is it necessary to wait for the delivery staff to collect the bottles? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<p>You can contact us and provide the number of deposit bottles you wish to get a refund for, your bank account number (the account holder’s name should match the name in the membership information), so we can transfer the refund to you. We will notify you of the date when our staff will collect the bottles and process to transfer you the refund within 18 working days from the day we receive the returned bottles.</p>
								</div>
							</div>
							
							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 4">4.4 I want to discontinue service, but I have some coupons left. Can I refund these coupons? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<ol class="description">
										<li>ราคาไม่รวม VAT 7%</li>
										<li class="text-wrap">*ราคาสำหรับพื้นที่จัดส่งในเขตกรุงเทพฯและปริมณฑลเท่านั้น สำหรับพื้นที่จัดส่งพัทยา <a href="http://sprinkle-th.com/contact.php" >กรุณาติดต่อตัวแทนจำหน่าย</a></li>
									</ol>
								</div>	
							</div>
							
							<!-- more 4 hidden -->
							
							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 5">4.5 How can I discontinue my Sprinkle service and receive my deposit? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<p>You can contact us and provide the number of deposit bottles you wish to get a refund for, your bank account number (the account holder’s name should match the name in the membership information), so we can transfer the refund to you. We will notify you of the date when our staff will collect the bottles and process to transfer you the refund within 18 working days from the day we receive the returned bottles.</p>
								</div>
							</div>
							
							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 6">4.6 Can I return some of the bottles but stay a member? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<ol class="description">
										<li>ราคาไม่รวม VAT 7%</li>
										<li class="text-wrap">*ราคาสำหรับพื้นที่จัดส่งในเขตกรุงเทพฯและปริมณฑลเท่านั้น สำหรับพื้นที่จัดส่งพัทยา <a href="http://sprinkle-th.com/contact.php" >กรุณาติดต่อตัวแทนจำหน่าย</a></li>
									</ol>
								</div>
							</div>
							
							<div class="ctrl-btn mt20-xs">
								<a class="sw-showmore" href="javascript:;" title="show more">show more</a>
							</div>
						</div>
					</div>
					<!-- q5 -->
					<div class="faq-bx" id="q5">
						<h3 class="h-topic txt-c mb15-xs mb30-sm">Contact information</h3>
						<div class="accordion">
							<div class="bx expand">
								<a class="acc-h active" href="javascript:;" title="question 1">5.1 How can I discontinue my Sprinkle service and receive my deposit? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane active" style="display: block;">
									<p>You can contact us and provide the number of deposit bottles you wish to get a refund for, your bank account number (the account holder’s name should match the name in the membership information), so we can transfer the refund to you. We will notify you of the date when our staff will collect the bottles and process to transfer you the refund within 18 working days from the day we receive the returned bottles.</p>
								</div>
							</div>
							
							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 2">5.2 Can I return some of the bottles but stay a member? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<ol class="description">
										<li>ราคาไม่รวม VAT 7%</li>
										<li class="text-wrap">*ราคาสำหรับพื้นที่จัดส่งในเขตกรุงเทพฯและปริมณฑลเท่านั้น สำหรับพื้นที่จัดส่งพัทยา <a href="http://sprinkle-th.com/contact.php" >กรุณาติดต่อตัวแทนจำหน่าย</a></li>
									</ol>
								</div>
							</div>

							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 3">5.3 Is it necessary to wait for the delivery staff to collect the bottles? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<p>You can contact us and provide the number of deposit bottles you wish to get a refund for, your bank account number (the account holder’s name should match the name in the membership information), so we can transfer the refund to you. We will notify you of the date when our staff will collect the bottles and process to transfer you the refund within 18 working days from the day we receive the returned bottles.</p>
								</div>
							</div>
							
							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 4">5.4 I want to discontinue service, but I have some coupons left. Can I refund these coupons? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<ol class="description">
										<li>ราคาไม่รวม VAT 7%</li>
										<li class="text-wrap">*ราคาสำหรับพื้นที่จัดส่งในเขตกรุงเทพฯและปริมณฑลเท่านั้น สำหรับพื้นที่จัดส่งพัทยา <a href="http://sprinkle-th.com/contact.php" >กรุณาติดต่อตัวแทนจำหน่าย</a></li>
									</ol>
								</div>	
							</div>
							
							<!-- more 4 hidden -->
							
							<div class="bx">
								<a class="acc-h" href="javascript:;" title="question 5">5.5 How can I discontinue my Sprinkle service and receive my deposit? <i class="icon-toggle"><img src="assets/imgs/ic-acc-down.png"></i></a>
								<div class="pane">
									<p>You can contact us and provide the number of deposit bottles you wish to get a refund for, your bank account number (the account holder’s name should match the name in the membership information), so we can transfer the refund to you. We will notify you of the date when our staff will collect the bottles and process to transfer you the refund within 18 working days from the day we receive the returned bottles.</p>
								</div>
							</div>
							
							
							<div class="ctrl-btn mt20-xs">
								<a class="sw-showmore" href="javascript:;" title="show more">show more</a>
							</div>
						</div>
					</div>
					<!--  end -->
					
					
				</div>
				<div class="btn-b2faq">
					<a href="javascript:;" title="Back to FAQ"><i class="fas fa-angle-left"></i> Back to FAQ</a>
				</div>
			</div>
			
			<!-- import contact -->
			<div class="ft-contact row _chd-cl-xs-12-sm-10-md-07-lg-06 center-xs">
				<div class="form-contact mb30-xs mb0-lg">
					<div class="title txt-c">
						<h2 class="h-topic">Did not find an answer?</h2>
						<p>We are here to help and answer any question you might have. <br>Fill out the form and we will contact you as soon as possible.</p>
					</div>
					<form method="post" action="" class="d-flex center-xs">
					<fieldset class="_self-cl-xs-12-sm-11 txt-l">
						<legend class="hid">Contact Us</legend>
						<div class="row _chd-cl-xs-12-xsh-06">
							<div class="rw">
								<label for="fname">First name<span class="rq">*</span></label>
								<input type="text" class="txt-box" id="fname" name="fname" placeholder="">
							</div>
							<div class="rw">
								<label for="lname">Last name<span class="rq">*</span></label>
								<input type="text" class="txt-box" id="lname" name="lname" placeholder="">
							</div>
						</div>
						
						<div class="row _chd-cl-xs-12-xsh-06">
							<div class="rw">
								<label for="mobile">Mobile No.<span class="rq">*</span></label>
								<input type="tel" class="txt-box" id="mobile" name="mobile" placeholder="">
							</div>
							<div class="rw">
								<label for="email">E-mail<span class="rq">*</span></label>
								<input type="email" class="txt-box" id="email" name="email" placeholder="">
							</div>
						</div>
						
						<div class="rw">
							<label for="subject">Subject</label>
							<!--<input type="text" class="txt-box" id="subject" name="subject" placeholder="">-->
							<select id="topic" name="topic" class="select2" data-placeholder="กรุณาเลือกเรื่องที่ต้องการติดต่อ">
							  <option></option>
							  <option value="1">ขอข้อมูลต่างๆเกี่ยวกับบริษัท</option>
								<option value="2">ติชมแสดงความคิดเห็นทั่วไป</option>
								<option value="3">ร้องเรียนปัญหาสินค้า</option>
								<option value="4">ร้องเรียนปัญหาด้านบริการ</option>
								<option value="5">สมัครงาน</option>
							</select>
						</div>
						
						<div class="rw">
							<label for="msg">Message</label>
							<textarea type="text" class="txt-box" id="msg" name="msg" placeholder=""></textarea>
						</div>
						
						<div class="ctrl-btn d-flex center-xs _chd-cl-xs-11-md-09-lg-08">
							<button type="submit" id="send-form" name="send-form" class="ui-btn-primary btn-block btn-lg">send message</button>
						</div>
					</fieldset>
				</form>
				</div>
			</div>
			<!-- /import contact -->

		</div>
	</section>
	
		
</div>

<?php include("incs/footer.html"); ?>

<!-- Java Script -->
<?php include("incs/js.html"); ?>
<script type="text/javascript" src="assets/js/jquery.idTabs.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	//expand step2
	$('.faq-mob .idTabs>li a').click(function(){
		$('.faq-mob .idTabs>li').removeClass('active');
		$(this).parent().toggleClass('active');
		$(this).parents('.faq-mob').addClass('step2');
	});
	//remove step2
	$('.btn-b2faq').click(function(){
		$('.faq-mob .idTabs>li').removeClass('active');
		$(this).parents('.faq-mob').removeClass('step2');
	});
	//showmore
	$('.sw-showmore').click(function(){
		$(this).fadeOut(100);
		$(this).parents('.accordion').toggleClass('expand');
	});
});
</script>
<!-- /JS -->
</body>
</html>
