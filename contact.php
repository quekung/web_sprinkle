<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html"); ?>
<!-- /Top Head -->
<script>
  //<![CDATA[
  $(document).ready(function(){
      $('#navigation>ul>li>a').removeClass('selected');
	  $('#navigation>ul>li:nth-child(5)>a').addClass('selected');
  });
  //]]>
</script>
<body class="page-detail">
<!-- header -->
<?php include("incs/header.html"); ?>
<!-- /header -->
<div id="toc" class="warpper">
	
	
	<section id="home" class="sec-contact">
		<div class="container">
			<div class="title txt-c">
				<h2 class="h-topic">Get in touch</h2>
				<p>We are here to help and answer any question you might have. Fill out the form and <br>we will contact you as soon as possible.</p>
			</div>
			
			<div class="followus">
				<ul class="ft-social _chd-cl-xs-06 _chd-cl-xsh">
						<li>
						<a href="#" title="link">
							<i><img src="assets/imgs/ft-ic-call.png" alt="email"></i>
							<div>
								<b>Tel: 02-712-7272</b>
								<span>Call Center</span>
							</div>
						</a>
						</li>
						<li>
						<a href="#" title="link">
							<i><img src="assets/imgs/ft-ic-mail.png" alt="email"></i>
							<div>
								<b>info@sprinkle-th.com</b>
								<span>Email</span>
							</div>
						</a>
						</li>
						<li>
						<a href="#" title="link">
							<i><img src="assets/imgs/ft-ic-LINE.png" alt="LINE"></i>
							<div>
								<b>@sprinklewater</b>
								<span>LINE</span>
							</div>
						</a>
						</li>
						<li>
						<a href="#" title="link">
							<i><img src="assets/imgs/ft-ic-FB.png" alt="fb: SprinkleDrinkingWater"></i>
							<div>
								<b>SprinkleDrinkingWater</b>
								<span>Facebook</span>
							</div>
						</a>
						</li>
						<li>
						<a href="#" title="link">
							<i><img src="assets/imgs/ft-ic-ig.png" alt="Instagram"></i>
							<div>
								<b>SprinkleDrinking</b>
								<span>Instagram</span>
							</div>
						</a>
						</li>
						<li>
						<a href="#" title="link">
							<i><img src="assets/imgs/ft-ic-youtube.png" alt="YouTube"></i>
							<div>
								<b>Sprinkle-channel</b>
								<span>YouTube</span>
							</div>
						</a>
						</li>
					</ul>
			</div>
			
			<div class="row _chd-cl-xs-12-sm-10-md-09-lg-06 center-xs">
				<div class="form-contact mb30-xs mb0-lg">
				<form method="post" action="" class="d-flex center-xs">
					<fieldset class="_self-cl-xs-12-sm-11-md-10 txt-l">
						<legend class="hid">Contact Us</legend>
						<div class="row _chd-cl-xs-12-xsh-06">
							<div class="rw">
								<label for="fname">First name<span class="rq">*</span></label>
								<input type="text" class="txt-box" id="fname" name="fname" placeholder="">
							</div>
							<div class="rw">
								<label for="lname">Last name<span class="rq">*</span></label>
								<input type="text" class="txt-box" id="lname" name="lname" placeholder="">
							</div>
						</div>
						
						<div class="row _chd-cl-xs-12-xsh-06">
							<div class="rw">
								<label for="mobile">Mobile No.<span class="rq">*</span></label>
								<input type="tel" class="txt-box" id="mobile" name="mobile" placeholder="">
							</div>
							<div class="rw">
								<label for="email">E-mail<span class="rq">*</span></label>
								<input type="email" class="txt-box" id="email" name="email" placeholder="">
							</div>
						</div>
						
						<div class="rw">
							<label for="subject">Subject</label>
							<!--<input type="text" class="txt-box" id="subject" name="subject" placeholder="">-->
							<select id="topic" name="topic" class="select2" data-placeholder="กรุณาเลือกเรื่องที่ต้องการติดต่อ">
							  <option></option>
							  <option value="1">ขอข้อมูลต่างๆเกี่ยวกับบริษัท</option>
								<option value="2">ติชมแสดงความคิดเห็นทั่วไป</option>
								<option value="3">ร้องเรียนปัญหาสินค้า</option>
								<option value="4">ร้องเรียนปัญหาด้านบริการ</option>
								<option value="5">สมัครงาน</option>
							</select>
						</div>
						
						<div class="rw">
							<label for="msg">Message</label>
							<textarea type="text" class="txt-box" id="msg" name="msg" placeholder=""></textarea>
						</div>
						
						<div class="ctrl-btn d-flex center-xs _chd-cl-xs-11-md-09-lg-08">
							<button type="submit" id="send-form" name="send-form" class="ui-btn-primary btn-block btn-lg">send message</button>
						</div>
					</fieldset>
				</form>
				</div>
				<div class="address txt-l">
					<h3>Sales office</h3>
					<address>
					M. Water Co., Ltd.<br>
283/50 Home Place Office Building,<br>
11/Flr. Soi Sukhumvit 55 (Thonglor 13),<br>
Sukhumvit Road, Kwaeng Klongtonnua,<br>
Khet Wattana, Bangkok 10110
					</address>
					<ul class="ct-link">
						<li><a href="tel:027127272"><u>Tel: 02-712-7272</u></a></li>
						<li><a href="tel:027127496"><u>Fax: 02-712-7496</u></a></li>
						<li><a href="mailto:info@sprinkle-th.com">Email: info@sprinkle-th.com </a></li>
					</ul>
				</div>
			</div>

		</div>
		<div class="ft-map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31005.347285312637!2d100.56085525282349!3d13.738514358848219!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29ee599368405%3A0x343c62b7bc2a80b1!2z4Lia4LiI4LiBLiDguYDguK3guYfguKEg4Lin4Lit4LmA4LiV4Lit4Lij4LmM!5e0!3m2!1sth!2sth!4v1617874672970!5m2!1sth!2sth" width="100%" height="600" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
		</div>
	</section>
	
		
</div>


<!-- Java Script -->
<?php include("incs/js.html"); ?>
<!-- /JS -->
</body>
</html>
