<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html"); ?>
<!-- /Top Head -->

<body class="page-home">
<!-- header -->
<?php include("incs/header.html"); ?>
<!-- /header -->
<div id="toc" class="warpper">
	
	
	<section id="product" class="sec-product">
		<div class="container">
			<h2 class="h-topic">รายการสินค้า <em class="t-blue">น้ำถัง 18.9L</em></h2>
			<div class="_chd-cl-xs-mb10">
			 <div><a href="#" title="xx" class="ui-btn">button default</a></div>
			 <div><a href="#" title="xx" class="ui-btn-primary">button default</a></div>
			 <div><a href="#" title="xx" class="ui-btn-red">button red</a></div>
			 <div><a href="#" title="xx" class="ui-btn-green">button green</a></div>
			 <div><a href="#" title="xx" class="ui-btn-blue">button blue</a></div>
			 <div><a href="#" title="xx" class="ui-btn-gray">button gray</a></div>
			 <div><a href="#" title="xx" class="ui-btn-light">button light</a></div>
			 <div><a href="#" title="xx" class="ui-btn-black">button black</a></div>
			 <div><a href="#" title="xx" class="ui-btn-dark">button dark</a></div>
			 <div><a href="#" title="xx" class="ui-btn-yellow">button yellow</a></div>
			 <div><a href="#" title="xx" class="ui-btn-orange">button orange</a></div>
			 <div><a href="#" title="xx" class="ui-btn-gold">button gold</a></div>
			 <div><a href="#" title="xx" class="ui-btn-border">button border</a></div>
			 <div><a href="#" title="xx" class="ui-btn-trans">button transparent</a></div>
			</div>
			
			<div>
				<span class="t-balck">text color</span>
				<span class="t-green">text color</span>
				<span class="t-gray">text color</span>
				<span class="t-red">text color</span>
				<span class="t-blue">text color</span>
				<span class="t-white bg-black">text color</span>
			</div>
			
			<div>
				<h1 class="h1">H1 heading</h1>
				<h1 class="h2">H2 heading</h1>
				<h1 class="h3">H3 heading</h1>
				<h1 class="h4">H4 heading</h1>
				<h1 class="h5">H5 heading</h1>
				<h1 class="h6">H6 heading</h1>
			</div>
			<?php /*?><ul class="list-pd">
				<li class="full">
					<div id="check-promotion" class="mz-chk">
						<!--<input type="checkbox" id="get-pro" name="get-pro">-->
						<label for="get-pro" class="d-inline"> 
							<div class="_flex between-xs">
								<span>โปรโมชั่น</span>
								<!--<em>สำหรับลูกค้าใหม่เท่านั้น!</em>-->
							</div>
						</label>
							<ol class="item-promote">
								<li>
									<div class="pd">
										<div id="check-promotion" class="mz-chk">
											<input type="radio" id="select-pro1" name="select-pro">
											<label for="select-pro1" class="d-flex flex-nowrap"> 
												<figure>
													<img src="di/product-promotion.png">
												</figure>
												<div class="info">
													<h3 class="title">
														<b>โปรโมชั่น 1.1 </b>
														<strong>x <em>24</em></strong>
														<span>คูปอง (ใบ)</span>
													</h3>
													<p class="price-average">65 บาท/ขวด</p>
													<p class="price-total">2,160.-</p>
													<div class="free">
														<img src="https://sprinkle-th.com/assets/img/general/promotion-01.png" alt="Free 5 Coupons">
													</div>
												</div>
											</label>
										</div>
										<div class="ft-detail">
											<a class="acc-h" href="javascript:;" title="รายละเอียด">รายละเอียด <i class="fas fa-info-circle"></i></a>
											<div class="pane">
												<ol class="description">
													<li>มัดจำถัง 3 ถัง</li>
													<li>ราคา 200 บาท/ถัง</li>
													<li>(จากราคาปกติ 300 บาท/ถัง)</li>
													<li>ราคาไม่รวม VAT 7%</li>
												</ol>
											</div>
										</div>
										<div class="for-newuser">สำหรับลูกค้าใหม่เท่านั้น!</div>
									</div>
								</li>
								
								<li>
									<div class="pd">
										<div id="check-promotion" class="mz-chk">
											<input type="radio" id="select-pro2" name="select-pro">
											<label for="select-pro2" class="d-flex flex-nowrap"> 
												<figure>
													<img src="di/product-promotion.png">
												</figure>
												<div class="info">
													<h3 class="title">
														<b>โปรโมชั่น 1.2 </b>
														<strong>x <em>24</em></strong>
														<span>คูปอง (ใบ)</span>
													</h3>
													<p class="price-average">65 บาท/ขวด</p>
													<p class="price-total">2,160.-</p>
													<div class="free">
														<img src="https://sprinkle-th.com/assets/img/general/promotion-02.png" alt="Free 3 Coupons and Water pump">
													</div>
												</div>
											</label>
										</div>
										<div class="ft-detail">
											<a class="acc-h" href="javascript:;" title="รายละเอียด">รายละเอียด <i class="fas fa-info-circle"></i></a>
											<div class="pane">
												<ol class="description">
													<li>มัดจำถัง 3 ถัง</li>
													<li>ราคา 200 บาท/ถัง</li>
													<li>(จากราคาปกติ 300 บาท/ถัง)</li>
													<li>ราคาไม่รวม VAT 7%</li>
												</ol>
											</div>
										</div>
										<div class="for-newuser">สำหรับลูกค้าใหม่เท่านั้น!</div>
									</div>
								</li>
								
								<li>
									<div class="pd">
										<div id="check-promotion" class="mz-chk">
											<input type="radio" id="select-pro3" name="select-pro">
											<label for="select-pro3" class="d-flex flex-nowrap"> 
												<figure>
													<img src="di/product-promotion.png">
												</figure>
												<div class="info">
													<h3 class="title">
														<b>โปรโมชั่น 2 </b>
														<strong>x <em>12</em></strong>
														<span>คูปอง (ใบ)</span>
													</h3>
													<p class="price-average">67 บาท/ขวด</p>
													<p class="price-total">1,404.-</p>
													<div class="free">
														<img src="https://sprinkle-th.com/assets/img/general/promotion-03.png" alt="Free Water pump">
													</div>
												</div>
											</label>
										</div>
										<div class="ft-detail">
											<a class="acc-h" href="javascript:;" title="รายละเอียด">รายละเอียด <i class="fas fa-info-circle"></i></a>
											<div class="pane">
												<ol class="description">
													<li>มัดจำถัง 3 ถัง</li>
													<li>ราคา 200 บาท/ถัง</li>
													<li>(จากราคาปกติ 300 บาท/ถัง)</li>
													<li>ราคาไม่รวม VAT 7%</li>
												</ol>
											</div>
										</div>
										<div class="for-newuser">สำหรับลูกค้าใหม่เท่านั้น!</div>
									</div>
								</li>
							</ol>					
					</div>
				</li>
				<li>
					<div class="pd">
						<div id="check-promotion" class="mz-chk">
							<input type="checkbox" id="get-pd1" name="get-pd1">
							<label for="get-pd1">
								<figure>
									<img src="https://sprinkle-th.com/assets/img/general/product-7x24.png">
								</figure>
								
							</label>
							<div class="wrap">
								<div class="info">
									<h3 class="title">
										คูปองน้ำดื่ม สปริงเคิล 24 ใบ
									</h3>
									<p class="price-average">65 บาท/ใบ</p>
								</div>
								<div class="bt-detail">
									<div class="box__number">
										<a class="decrease" onClick="if ($(this).parents('.mz-chk').children('input').not(':checked')){$(this).parents('.mz-chk').children('input').prop('checked',true);}">-</a>
										<input type="text" class="number" readonly="readonly" name="coupon24" id="coupon24" value="0">
										<a class="increase" onClick="if ($(this).parents('.mz-chk').children('input').not(':checked')){$(this).parents('.mz-chk').children('input').prop('checked',true);}">+</a>
									</div>
									<span class="price">1,560.-</span>
								</div>
							</div>

							</div>
							<div class="ft-detail">
								<a class="acc-h" href="javascript:;" title="รายละเอียด">รายละเอียด <i class="fas fa-info-circle"></i></a>
								<div class="pane">
									<ol class="description">
										<li>ราคาไม่รวม VAT 7%</li>
										<li class="text-wrap">*ราคาสำหรับพื้นที่จัดส่งในเขตกรุงเทพฯและปริมณฑลเท่านั้น สำหรับพื้นที่จัดส่งพัทยา <a href="http://sprinkle-th.com/contact.php" >กรุณาติดต่อตัวแทนจำหน่าย</a></li>
									</ol>
								</div>
							</div>
					</div>
				</li>
				<li>
					<div class="pd">
						<div id="check-promotion" class="mz-chk">
							<input type="checkbox" id="get-pd2" name="get-pd2">
							<label for="get-pd2">
								<figure>
									<img src="https://sprinkle-th.com/assets/img/general/product-8x12.png">
								</figure>
							</label>
							<div class="wrap">
								<div class="info">
									<h3 class="title">
										คูปองน้ำดื่ม สปริงเคิล 12 ใบ
									</h3>
									<p class="price-average">67 บาท/ใบ</p>
								</div>
								<div class="bt-detail">
									<div class="box__number">
										<a class="decrease" onClick="if ($(this).parents('.mz-chk').children('input').not(':checked')){$(this).parents('.mz-chk').children('input').prop('checked',true);}">-</a>
										<input type="text" class="number" readonly="readonly" name="coupon12" id="coupon12" value="0">
										<a class="increase" onClick="if ($(this).parents('.mz-chk').children('input').not(':checked')){$(this).parents('.mz-chk').children('input').prop('checked',true);}">+</a>
									</div>
									<span class="price">804.-</span>
								</div>
							</div>

							</div>
							<div class="ft-detail">
								<a class="acc-h" href="javascript:;" title="รายละเอียด">รายละเอียด <i class="fas fa-info-circle"></i></a>
								<div class="pane">
									<ol class="description">
										<li>ราคาไม่รวม VAT 7%</li>
										<li class="text-wrap">*ราคาสำหรับพื้นที่จัดส่งในเขตกรุงเทพฯและปริมณฑลเท่านั้น สำหรับพื้นที่จัดส่งพัทยา <a href="http://sprinkle-th.com/contact.php" >กรุณาติดต่อตัวแทนจำหน่าย</a></li>
									</ol>
								</div>
							</div>
					</div>
				</li>
				<li>
					<div class="pd">
						<div id="check-promotion" class="mz-chk">
							<input type="checkbox" id="get-pd3" name="get-pd3">
							<label for="get-pd3">
								<figure>
									<img src="https://sprinkle-th.com/assets/img/general/BOTTLE-sh.jpg">
								</figure>
							</label>
							<div class="wrap">
								<div class="info">
									<h3 class="title">
										มัดจำถัง
									</h3>
									<!--<p class="price-average">67 บาท/ใบ</p>-->
								</div>
								<div class="bt-detail">
									<div class="box__number">
										<a class="decrease" onClick="if ($(this).parents('.mz-chk').children('input').not(':checked')){$(this).parents('.mz-chk').children('input').prop('checked',true);}">-</a>
										<input type="text" class="number" readonly="readonly" name="deposit" id="deposit" value="0">
										<a class="increase" onClick="if ($(this).parents('.mz-chk').children('input').not(':checked')){$(this).parents('.mz-chk').children('input').prop('checked',true);}">+</a>
									</div>
									<span class="price">200.-</span>
								</div>
							</div>

							</div>
							<div class="ft-detail">
								<a class="acc-h" href="javascript:;" title="รายละเอียด">รายละเอียด <i class="fas fa-info-circle"></i></a>
								<div class="pane">
									<ol class="description">
										<li>ราคารวม VAT 7%</li>
										<li class="text-wrap">*ราคาสำหรับพื้นที่จัดส่งในเขตกรุงเทพฯและปริมณฑลเท่านั้น สำหรับพื้นที่จัดส่งพัทยา <a href="http://sprinkle-th.com/contact.php" >กรุณาติดต่อตัวแทนจำหน่าย</a></li>
									</ol>
								</div>
							</div>
					</div>
				</li>
				<li>
					<div class="pd">
						<div id="check-promotion" class="mz-chk">
							<input type="checkbox" id="get-pd4" name="get-pd4">
							<label for="get-pd4">
								<figure>
									<img src="https://sprinkle-th.com/assets/img/general/pump_USB_PNG.png">
								</figure>
							</label>
							<div class="wrap">
								<div class="info">
									<h3 class="title">
										 เครื่องปั๊มน้ำอัตโนมัติ (TOUCH SMART)
									</h3>
									<p class="price-average">ส่วนลด 10% สำหรับสมาชิก</p>
								</div>
								<div class="bt-detail">
									<div class="box__number">
										<a class="decrease" onClick="if ($(this).parents('.mz-chk').children('input').not(':checked')){$(this).parents('.mz-chk').children('input').prop('checked',true);}">-</a>
										<input type="text" class="number" readonly="readonly" name="pump001" id="pump001" value="0">
										<a class="increase" onClick="if ($(this).parents('.mz-chk').children('input').not(':checked')){$(this).parents('.mz-chk').children('input').prop('checked',true);}">+</a>
									</div>
									<span class="price">400.-</span>
								</div>
							</div>

							</div>
							<div class="ft-detail">
								<a class="acc-h" href="javascript:;" title="รายละเอียด">รายละเอียด <i class="fas fa-info-circle"></i></a>
								<div class="pane">
									<ol class="description">
										<li>ราคาไม่รวม VAT 7%</li>
										<li><a href="https://youtu.be/eySSLzkWiQ8" target="_bank"  tabindex="0">ขั้นตอนการใช้งาน</a></li>
									</ol>
								</div>
							</div>
					</div>
				</li>
				<li>
					<div class="pd">
						<div id="check-promotion" class="mz-chk">
							<input type="checkbox" id="get-pd6" name="get-pd6">
							<label for="get-pd6">
								<figure>
									<img src="https://sprinkle-th.com/assets/img/general/product-12.png">
								</figure>
							</label>
							<div class="wrap">
								<div class="info">
									<h3 class="title">
										 BL 724 (ใส่ถังด้านล่าง)
									</h3>
									<p class="price-average">3 หัวก๊อก น้ำร้อน, น้ำเย็น และน้ำธรรมดา</p>
								</div>
								<div class="bt-detail">
									<div class="box__number">
										<a class="decrease" onClick="if ($(this).parents('.mz-chk').children('input').not(':checked')){$(this).parents('.mz-chk').children('input').prop('checked',true);}">-</a>
										<input type="text" class="number" readonly="readonly" name="BL" id="BL" value="0">
										<a class="increase" onClick="if ($(this).parents('.mz-chk').children('input').not(':checked')){$(this).parents('.mz-chk').children('input').prop('checked',true);}">+</a>
									</div>
									<span class="price">9,800.-</span>
								</div>
							</div>

							</div>
							<div class="ft-detail">
								<a class="acc-h" href="javascript:;" title="รายละเอียด">รายละเอียด <i class="fas fa-info-circle"></i></a>
								<div class="pane">
									<ol class="description">
										<li>ราคารวม VAT 7%</li>
										<li>รับประกัน คอมเพรสเซอร์ 5ปี อุปกรณ์ไฟฟ้า 1ปี</li>
									</ol>
								</div>
							</div>
					</div>
				</li>
				<li>
					<div class="pd">
						<div id="check-promotion" class="mz-chk">
							<input type="checkbox" id="get-pd7" name="get-pd7">
							<label for="get-pd7">
								<figure>
									<img src="https://sprinkle-th.com/assets/img/general/product-13.png">
								</figure>
							</label>
							<div class="wrap">
								<div class="info">
									<h3 class="title">
										 HC335 (พร้อมตู้เก็บของด้านล่าง)
									</h3>
									<p class="price-average">3 หัวก๊อก น้ำร้อน, น้ำเย็น และน้ำธรรมดา</p>
								</div>
								<div class="bt-detail">
									<div class="box__number">
										<a class="decrease" onClick="if ($(this).parents('.mz-chk').children('input').not(':checked')){$(this).parents('.mz-chk').children('input').prop('checked',true);}">-</a>
										<input type="text" class="number" readonly="readonly" name="HC" id="HC" value="0">
										<a class="increase" onClick="if ($(this).parents('.mz-chk').children('input').not(':checked')){$(this).parents('.mz-chk').children('input').prop('checked',true);}">+</a>
									</div>
									<span class="price">5,800.-</span>
								</div>
							</div>

							</div>
							<div class="ft-detail">
								<a class="acc-h" href="javascript:;" title="รายละเอียด">รายละเอียด <i class="fas fa-info-circle"></i></a>
								<div class="pane">
									<ol class="description">
										<li>ราคารวม VAT 7%</li>
										<li>รับประกัน คอมเพรสเซอร์ 5ปี อุปกรณ์ไฟฟ้า 1ปี</li>
									</ol>
								</div>
							</div>
					</div>
				</li>
				<li>
					<div class="pd">
						<div id="check-promotion" class="mz-chk">
							<input type="checkbox" id="get-pd8" name="get-pd8">
							<label for="get-pd8">
								<figure>
									<img src="https://sprinkle-th.com/assets/img/general/product-14.png">
								</figure>
							</label>
							<div class="wrap">
								<div class="info">
									<h3 class="title">
										TSHC170
									</h3>
									<p class="price-average">2 หัวก๊อก น้ำร้อน และน้ำเย็น </p>
								</div>
								<div class="bt-detail">
									<div class="box__number">
										<a class="decrease" onClick="if ($(this).parents('.mz-chk').children('input').not(':checked')){$(this).parents('.mz-chk').children('input').prop('checked',true);}">-</a>
										<input type="text" class="number" readonly="readonly" name="TSHC" id="TSHC" value="0">
										<a class="increase" onClick="if ($(this).parents('.mz-chk').children('input').not(':checked')){$(this).parents('.mz-chk').children('input').prop('checked',true);}">+</a>
									</div>
									<span class="price">5,550.-</span>
								</div>
							</div>

							</div>
							<div class="ft-detail">
								<a class="acc-h" href="javascript:;" title="รายละเอียด">รายละเอียด <i class="fas fa-info-circle"></i></a>
								<div class="pane">
									<ol class="description">
										<li>ราคารวม VAT 7%</li>
										<li>รับประกัน คอมเพรสเซอร์ 5ปี อุปกรณ์ไฟฟ้า 1ปี</li>
									</ol>
								</div>
							</div>
					</div>
				</li>
				<li>
					<div class="pd">
						<div id="check-promotion" class="mz-chk">
							<input type="checkbox" id="get-pd9" name="get-pd9">
							<label for="get-pd9">
								<figure>
									<img src="https://sprinkle-th.com/assets/img/general/product-15.png">
								</figure>
							</label>
							<div class="wrap">
								<div class="info">
									<h3 class="title">
										TSCO170
									</h3>
									<p class="price-average">น้ำเย็น 1 หัวก๊อก</p>
								</div>
								<div class="bt-detail">
									<div class="box__number">
										<a class="decrease" onClick="if ($(this).parents('.mz-chk').children('input').not(':checked')){$(this).parents('.mz-chk').children('input').prop('checked',true);}">-</a>
										<input type="text" class="number" readonly="readonly" name="TSCO" id="TSCO" value="0">
										<a class="increase" onClick="if ($(this).parents('.mz-chk').children('input').not(':checked')){$(this).parents('.mz-chk').children('input').prop('checked',true);}">+</a>
									</div>
									<span class="price">5,150.-</span>
								</div>
							</div>

							</div>
							<div class="ft-detail">
								<a class="acc-h" href="javascript:;" title="รายละเอียด">รายละเอียด <i class="fas fa-info-circle"></i></a>
								<div class="pane">
									<ol class="description">
										<li>ราคารวม VAT 7%</li>
										<li>รับประกัน คอมเพรสเซอร์ 5ปี อุปกรณ์ไฟฟ้า 1ปี</li>
									</ol>
								</div>
							</div>
					</div>
				</li>
				<!--<li>
					<div id="check-promotion" class="mz-chk">
						<input type="checkbox" id="get-pd3" name="get-pd3">
						<label for="get-pd3"> สินค้า 3</label>
					</div>
				</li>-->
			</ul><?php */?>
			
			<div id="check-bar" class="footer-bar _flex center-xs middle-xs">
				<button type="reset" class="ui-btn-white">ยกเลิก</button>
        		<a id="count-btn" href="cart.php?" title="สั่งซื้อ" class="ui-btn-blue">สั่งซื้อ <i class="noti"><div class="none">0</div></i></a>

			</div>

		</div>
	</section>
	
		
</div>


<?php include("incs/footer.html"); ?>

<!-- Java Script -->
<?php include("incs/js.html"); ?>
<!-- /JS -->
</body>
</html>
