<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html"); ?>
<!-- /Top Head -->

<body class="page-thk">
<!-- header -->
<?php include("incs/header.html"); ?>
<!-- /header -->
<div id="toc" class="wrapper">
	
	
	<section id="thank" class="sec-thank">
		<div class="container">

		  <main class="main">
			<img class="thank-you-image" src="assets/imgs/thank_you_image.svg" alt="Thank you">
			<h2 class="thank-you-title">ได้รับคำสั่งซื้อเรียบร้อยแล้ว!</h2>
			<p class="thank-you-description">ขอบคุณที่ใช้บริการสปริงเคิล</p>
			
			<div class="d-flex _chd-cl-xs-10-sm-08-md-06 center-xs">
				<a class="ui-btn-primary btn-block" href="products.php" title="back to catalog">BACK TO CATALOG</a>
			</div>
		  </main>

		</div>
	</section>
	
		
</div>


<!-- Java Script -->
<?php include("incs/js.html"); ?>
<!-- /JS -->
</body>
</html>
