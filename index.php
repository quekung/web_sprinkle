<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html"); ?>
<!-- /Top Head -->

<body class="page-home">
<!-- header -->
<?php include("incs/header.html"); ?>
<!-- /header -->
<div id="toc" class="warpper">
	
	
	<section id="home" class="sec-home">
		<div class="container">
			<div class="title">
				<h2 class="mb20-xs mb30-md">Mater of bringing <br>Water to life</h2>
				<div><a class="ui-btn-primary btn-lg" href="products.php" title="order now">ORDER NOW</a></div>
			</div>
			
			<!-- Flickity HTML init -->
			<div class="home-link carousel" data-flickity='{ "cellAlign": "left", "contain": true, "prevNextButtons": false, "pageDots": false, "autoPlay": true }'>
			  <div class="carousel-cell">
			  <a href="products.php" title="Online Ordering">
			  	<img src="assets/imgs/slider/item_slider1.jpg" alt="Online Ordering">
			  </a>
			  </div>
			  <div class="carousel-cell">
			  <a href="#vdoSprinkle" data-fancybox="Highlight" title="Corporate sprinkle">
			  	<img src="assets/imgs/slider/item_slider2.jpg" alt="Corporate sprinkle">
			  </a>
			  </div>
			  <div class="carousel-cell">
			  <a href="#vdoPUMP" data-fancybox="Highlight" title="Water PUMP">
			  	<img src="assets/imgs/slider/item_slider3.jpg" alt="Water PUMP">
			  </a>
			  </div>
			  <div class="carousel-cell">
			  <a href="assets/imgs/example-slide-12_big.jpg" data-fancybox="Highlight" data-caption="มาช่วยกันปิดฝาถังน้ำดื่มสปริงเคิลกันเถอะ" title="มาช่วยกันปิดฝาถังน้ำดื่มสปริงเคิลกันเถอะ">
			  	<img src="assets/imgs/slider/item_slider4.jpg" alt="มาช่วยกันปิดฝาถังน้ำดื่มสปริงเคิลกันเถอะ">
			  </a>
			  </div>
			</div>
			
			<video width="1024" height="640" controls id="vdoSprinkle" style="display:none;">
				<source src="https://sprinkle-th.com/images/banner/VDO_Corporate_sprinkle_FinalVersion2_.mp4" type="video/mp4">
				Your browser doesn't support HTML5 video tag.
			</video>
			
			<video width="1024" height="640" controls id="vdoPUMP" style="display:none;">
				<source src="https://sprinkle-th.com/images/banner/VIDEO-PUMP.mp4" type="video/mp4">
				Your browser doesn't support HTML5 video tag.
			</video>

		</div>
	</section>
	
		
</div>


<!-- Java Script -->
<?php include("incs/js.html"); ?>
<!-- /JS -->
</body>
</html>
