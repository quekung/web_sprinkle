<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html"); ?>
<!-- /Top Head -->
<script>
  //<![CDATA[
  $(document).ready(function(){
      $('#navigation>ul>li>a').removeClass('selected');
	  $('#navigation>ul>li:nth-child(2)>a').addClass('selected');
  });
  //]]>
</script>
<body class="page-products">
<!-- header -->
<?php include("incs/header.html"); ?>
<!-- /header -->
<div id="toc" class="warpper">
	
	
	<section id="home" class="sec-pd">
		<div class="full">
			<div class="topbanner banner-top">
				<a href="#" title="banner">
				<div class="hidden-xs"><img src="assets/imgs/banner_desktop.png" alt="1692x260"></div>
				<div class="visible-xs"><img src="assets/imgs/banner_mob.png" alt="340x280"></div>
				</a>
			</div>
			
			<ul class="list-pd _chd-cl-xs-06-md-04-lg-03-xl-25">
				<?php for($i=1; $i<=15; $i++) { 
				
				  $img_url = "assets/products/deposit.png";
				  $name = "Sprinkle Water";
				  $price = "0.-";
				  switch($i)
				  {
					  case "1": 
								  $img_url = "assets/products/350ml.png";
								  $name = "Sprinkle 350ml.";
								  $price = "฿75.-";
						  break;
					  case "2": 
								  $img_url = "assets/products/550ml.png";
								  $name = "Sprinkle 550ml.";
								  $price = "฿70.-";
						  break;
					  case "3": 
								  $img_url = "assets/products/1.5L.png";
								  $name = "Sprinkle 1.5L.";
								  $price = "฿66.50.-";
						  break;
					  case "4": 
								  $img_url = "assets/products/6l.png";
								  $name = "Sprinkle 6L";
								  $price = "฿149.-";
						  break;
					  case "7": 
								  $img_url = "assets/products/coupons24.png";
								  $name = "Sprinkle Bottled Water \n Coupon x24";
								  $price = "฿1,560.-";
						  break;
					  case "8": 
								  $img_url = "assets/products/coupons12.png";
								  $name = "Sprinkle Bottled Water \n Coupon x12";
								  $price = "฿804.-";
						  break; 
					  case "9": 
								  $img_url = "assets/products/deposit.png";
								  $name = "Deposit Bottle";
								  $price = "฿200.-";
						  break;
					  case "10": 
								  $img_url = "assets/products/auto-water-pump.png";
								  $name = "Automatic Water Pump";
								  $price = "฿270.-";
						  break;
					  case "11": 
								  $img_url = "assets/products/Smart-Dispenser.png";
								  $name = "Touch Smart Electric \n Water Dispenser";
								  $price = "฿360.-";
						  break;
					  case "12": 
								  $img_url = "assets/products/BL724.png";
								  $name = "BL 724";
								  $price = "฿9,800.-";
						  break;
					  case "13": 
								  $img_url = "assets/products/HC335.png";
								  $name = "HC 335";
								  $price = "฿5,800.-";
						  break;
					  case "14": 
								  $img_url = "assets/products/TSCO170.png";
								  $name = "TSHC170";
								  $price = "฿5,550.-";
						  break;
					  case "15": 
								  $img_url = "assets/products/TSHC170.png";
								  $name = "TSCO170";
								  $price = "฿5,150.-";
						  break;
}
				
				if($i==5) {
				?>
				<li class="pro">
					<div class="pd">
							<header>
								<h2>Promotion #1</h2>
								<p class="t-blue">new member only</p>
							</header>
							<div class="chk-promotion d-flex flex-nowrap">
								<figure>
									<img src="assets/products/18.9L.png" alt="18.9L">
									<div class="img-free">
										<img src="assets/imgs/pro-free-coupon.png">
									</div>
								</figure>
								<div class="free">
									<b>FREE</b>
									<div class="select-button">
										<input type="radio" id="pro1-1" name="pro1" checked>
										<label for="pro1-1"><i class="icon"><img src="assets/imgs/ic-coupons.png" height="14"></i> 5 coupons</label>
									</div>
									<span>OR</span>
									<div class="select-button">
										<input type="radio" id="pro1-2" name="pro1">
										<label for="pro1-2" class="d-flex center-xs">
										<p class="txt-l d-inline"><i class="icon"><img src="assets/imgs/ic-coupons.png" height="14"></i> 3 coupons +</p>
										<p class="txt-l d-inline"><i class="icon"><img src="assets/imgs/ic-pump.png" height="14"></i> Touch Smart <br>electric <br>water <br>dispenser</p>
										</label>
									</div>
								</div>
							</div>
							<a data-fancybox data-src="#pd-d0<?php echo($i) ?>" href="javascript:;">
								<div class="info">
									<h3 class="title">
										x24 Coupons
									</h3>
									<p class="price-average">64 baht/coupon <span class="t-blue">details</span></p>
								</div>
							</a>
							<div class="wrap">
								<div class="pro-text">
									bottle deposit of 3 bottles 200 Baht/bottle 
									<del>(300 Baht/bottle)</del>
								</div>
								<div class="bt-detail d-flex between-xs middle-xs">

									<span class="price">
										<big class="d-block">฿2,160.-</big>
										<small>Excluding VAT 7%</small>
									</span>
									
									<button class="ui-btn-border btn-addcart" type="button" id="add-pd<?php echo($i) ?>"><span>ADD TO</span> <img src="assets/imgs/ic-addcart.png" height="20"></button>
									
								</div>
							</div>
							<div id="pd-d0<?php echo($i) ?>" class="ft-detail" style="display: none">
								<div class="head-title d-flex start-xs">
										<figure>
											<img src="assets/products/18.9L-pro.png" alt="18.9L">
										</figure>
										<div class="pd-name">
											<h3 class="title">
												Promotion (x24 Coupons)
											</h3>
											<p class="price-average">64 baht/bottle</p>
										</div>
									</div>

									<div class="type-purchase">
										<p>Choose free promotion: </p>
										<div class="d-flex flex-column">
											<div class="mz-chk mb10-xs">
												<input name="promotion1" id="promotion1-1" value="0" type="radio" checked>
												<label for="promotion1-1">5 coupons</label>
											</div>
											<div class="mz-chk">
												<input name="promotion1" id="promotion1-2" value="1" type="radio">
												<label for="promotion1-2">3 coupons + Automatic water pump</label>
											</div>
										</div>
									</div>
									<div class="text-detail">
										bottle deposit of 3 bottles 200 Baht/bottle <del class="t-gray">(300 Baht/bottle)</del>
									</div>
									<div class="total-mob">
										<b>Total:</b>
										<div class="price">
											<big class="total_price">฿2,160.-</big>
											<small>Includinng VAT 7%</small>
										</div>
									</div>
									<div class="ctrl-btn">
										<button type="button" class="ui-btn-primary btn-block btn-addcart" data-fancybox-close="">Add to cart</button>
									</div>
							</div>
					</div>
				</li>
				<?php } elseif($i==6) {?>
				<li class="pro">
					<div class="pd">
							<header>
								<h2>Promotion #2</h2>
								<p class="t-blue">new member only</p>
							</header>
							<div class="chk-promotion d-flex flex-nowrap">
								<figure>
									<img src="assets/products/18.9L.png" alt="18.9L">
									<div class="img-free">
										<img src="assets/imgs/pro-free-coupon.png">
									</div>
								</figure>
								<div class="free">
									<b>FREE</b>
									<div class="select-button h-100">
										<input type="radio" id="pro2" name="pro2" checked>
										<label for="pro2" class="h-100 d-flex middle-xs">
										<i class="icon"><img src="assets/imgs/ic-waterpump.png" height="24"></i> 
										<p class="mt10-xs">Touch Smart <br>electric <br>water<br> dispenser</p>
										</label>
									</div>
								</div>
							</div>
							<a data-fancybox data-src="#pd-d0<?php echo($i) ?>" href="javascript:;">
								<div class="info">
									<h3 class="title">
										x12 Coupons
									</h3>
									<p class="price-average">64 baht/coupon<span class="t-blue">details</span></p>
								</div>
							</a>
							<div class="wrap">
								<div class="pro-text">
									bottle deposit of 3 bottles 200 Baht/bottle 
									<del>(300 Baht/bottle)</del>
								</div>
								<div class="bt-detail d-flex between-xs middle-xs">

									<span class="price">
										<big class="d-block">฿2,160.-</big>
										<small>Excluding VAT 7%</small>
									</span>
									
									<button class="ui-btn-border btn-addcart" type="button" id="add-pd<?php echo($i) ?>"><span>ADD TO</span> <img src="assets/imgs/ic-addcart.png" height="20"></button>
									
								</div>
							</div>
							<div id="pd-d0<?php echo($i) ?>" class="ft-detail" style="display: none">
								<div class="head-title d-flex start-xs">
										<figure>
											<img src="assets/products/18.9L-pro.png" alt="18.9L">
										</figure>
										<div class="pd-name">
											<h3 class="title">
												Promotion (x12 Coupons)
											</h3>
											<p class="price-average">64 baht/bottle</p>
										</div>
									</div>

									<div class="type-purchase">
										<p>Choose free promotion: </p>
										<div class="d-flex flex-column">
											<div class="mz-chk mb10-xs">
												<input name="promotion2" id="promotion2" value="0" type="radio" checked>
												<label for="promotion2">Touch Smart electric water dispenser</label>
											</div>
											
										</div>
									</div>
									<div class="text-detail">
										bottle deposit of 3 bottles 200 Baht/bottle <del class="t-gray">(300 Baht/bottle)</del>
									</div>
									<div class="total-mob">
										<b>Total:</b>
										<div class="price">
											<big class="total_price">฿2,160.-</big>
											<small>Includinng VAT 7%</small>
										</div>
									</div>
									<div class="ctrl-btn">
										<button type="button" class="ui-btn-primary btn-block btn-addcart" data-fancybox-close="">Add to cart</button>
									</div>
							</div>
					</div>
				</li>
				<?php } else { ?>
				
				
				<li>
					<div class="pd">
							<a data-fancybox data-src="#pd-d0<?php echo($i) ?>" href="javascript:;">
								<figure>
									<img src="<?php echo $img_url; ?>" alt="<?php echo $name; ?>">
								</figure>
								<div class="info">
									<h3 class="title">
										<?php echo nl2br($name); ?>
									</h3>
									<p class="price-average">18 bottles/pack <span class="t-blue">details</span></p>
								</div>
							</a>
							<div class="wrap">
								<div class="box__number">
										<a class="decrease">-</a>
										<input type="text" class="number" readonly="readonly" name="counter-pd<?php echo($i) ?>" id="counter-pd<?php echo($i) ?>" value="1">
										<a class="increase">+</a>
								</div>
								<div class="bt-detail d-flex between-xs middle-xs">

									<span class="price">
										<big class="d-block"><?php echo $price; ?></big>
										<small>Excluding VAT 7%</small>
									</span>
									
									<button class="ui-btn-border btn-addcart" type="button" id="add-pd<?php echo($i) ?>"><span>ADD TO</span> <img src="assets/imgs/ic-addcart.png" height="20"></button>
									
								</div>
							</div>
							<div id="pd-d0<?php echo($i) ?>" class="ft-detail" style="display: none">
									<div class="head-title d-flex start-xs">
										<figure>
											<img src="<?php echo $img_url; ?>" alt="<?php echo $name; ?>">
										</figure>
										<div class="pd-name">
											<h3 class="title">
												<?php echo nl2br($name); ?>
											</h3>
											<p class="price-average">18 bottles/pack</p>
										</div>
									</div>
									<div class="quantity d-flex between-xs">
										<div class="l">
											<p><b>Quantity:</b> (packs)</p>
											<span>- 18 bottles</span>
										</div>
										<div class="r pd">
											<div class="box__number">
													<a class="decrease">-</a>
													<input type="text" class="number" readonly="readonly" name="sub-counter-pd<?php echo($i) ?>" id="sub-counter-pd<?php echo($i) ?>" value="0">
													<a class="increase">+</a>
											</div>
										</div>
									</div>
									<div class="type-purchase">
										<p>Choose type of purchase:</p>
										<div class="d-flex pt5-xs">
											<div class="mz-chk mr20-xs">
												<input name="dv-purchase-<?php echo($i) ?>" id="d-onetime-<?php echo($i) ?>" value="0" type="radio" checked>
												<label for="d-onetime-<?php echo($i) ?>">One time order</label>
											</div>
											<div class="mz-chk">
												<input name="dv-purchase-<?php echo($i) ?>" id="d-subscribtion-<?php echo($i) ?>" value="1" type="radio">
												<label for="d-subscribtion-<?php echo($i) ?>">Subscribtion</label>
											</div>
										</div>
									</div>
									<div class="text-detail">
										Delivery cost 5 Baht/pack. Minimum order 2 packs. Order over 20 packs at 70 Baht/pack
									</div>
									<div class="total-mob">
										<b>Total:</b>
										<div class="price">
											<big class="total_price">฿3,145.87</big>
											<small>Includinng VAT 7%</small>
										</div>
									</div>
									<div class="ctrl-btn">
										<button type="button" class="ui-btn-primary btn-block btn-addcart" data-fancybox-close="">Add to cart</button>
									</div>
								</div>
					</div>
				</li>
				<?php }?>
				<?php } ?>


			</ul>
			

		</div>
	</section>
	
		
</div>

<?php include("incs/footer.html"); ?>

<!-- Java Script -->
<?php include("incs/js.html"); ?>
<!-- /JS -->
</body>
</html>
