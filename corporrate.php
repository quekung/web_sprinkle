<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html"); ?>
<!-- /Top Head -->
<script>
  //<![CDATA[
  $(document).ready(function(){
      $('#navigation>ul>li>a').removeClass('selected');
	  $('#navigation>ul>li:nth-child(3)>a').addClass('selected');
  });
  //]]>
</script>
<body class="page-detail">
<!-- header -->
<?php include("incs/header.html"); ?>
<!-- /header -->
<div id="toc" class="wrapper">
	
	
	<section id="home" class="sec-faq sec-corporate">
		<div class="container">
			
			<div class="cor-page-tabs">
				<ul class="idTabs">
					<li class="active"><a class="selected" href="#p1" title="About us">About us</a></li>
					<li><a href="#p2" title="Products types">Products types</a></li>
					<li><a href="#p3" title="Quality/process control">Quality/process control</a></li>
					<li><a href="#p4" title="Delivery">Delivery</a></li>
				</ul>
				<div class="contentTabs reader">
					<!-- p1 -->
					<div class="page-bx" id="p1">
						<h2 class="h-topic mb20-xs">From home-delivery water to the Master of Bringing Water to Life</h2>
						<figure class="mb30-xs">
							<a href="assets/imgs/cor-about-c1.jpg" data-fancybox="images"><img src="assets/imgs/cor-about-c1.jpg"></a>
						</figure>
						
						<div class="row _chd-cl-xs-12-xsh-06 between-sm pt30-sm">
							<figure class="thm-vdo">
								<a data-fancybox="" href="https://www.youtube.com/watch?v=kPOUB9hQhQI&amp;feature=emb_imp_woyt"><img src="assets/imgs/cor-about-c2.jpg"><i class="ic-vdo"></i></a>
							</figure>
							<div class="detail">
								<p>Looking back on 1985, we began our journey with the commitment to deliver quality drinking water to every household doorstep. At Sprinkle, we believe that a good quality of life starts with a good glass of drinking water. With our drinking water delivery experience of over 35 years, Sprinkle has adapted, improved, and modernized ourselves in technology, design, and latest innovations - ensuring the endless flow of quality and healthy daily life for all Thai people.</p>
							</div>
						</div>
					</div>
					<!-- p2 -->
					<div class="page-bx" id="p2">
						<h2 class="h-topic mb20-xs">How much Sprinkle do you need?</h2>
						<figure class="mb30-xs">
							<a href="assets/imgs/cor-about-c1.jpg" data-fancybox="images"><img src="assets/imgs/cor-products-c1.jpg"></a>
						</figure>
						
						<div class="row _chd-cl-xs-12-xsh-06 between-sm pt30-sm mb30-xs">
							<div class="detail">
								<p>The more the better or less equals more? Whatever is true to you, we have just the right offers that would fit different lifestyles. There are some bottles to keep the whole office hydrated and some to fit the athlete’s hand on the marathon.</p>
							</div>
							<div class="detail">
								<p>We all need water, however, in different amounts, and our solutions fit all kinds of needs. Scroll and find out!</p>
							</div>
						</div>
						
						<div id="pd18.9">
							<header>
								<h3><span>Sprinkle 18.9 L</span></h3>
								<nav>
									<ul class="d-flex center-xs around-sm">
										<li><i><img src="assets/imgs/nav-18.9-1.png"></i><small>Office</small></li>
										<li><i><img src="assets/imgs/nav-18.9-2.png"></i><small>Industry</small></li>
										<li><i><img src="assets/imgs/nav-18.9-3.png"></i><small>Home use</small></li>
										<li><i><img src="assets/imgs/nav-18.9-4.png"></i><small>Cooking</small></li>
										<li><i><img src="assets/imgs/nav-18.9-5.png"></i><small>Receptions</small></li>
									</ul>
								</nav>
							</header>

							<div class="row _chd-cl-xs-12-xsh-06 between-sm pt30-xs mb30-xs">
								<div class="detail">
									<p>18.9 liters of water is just enough to keep your team or family hydrated for quite a time. The more people - the more bottles you need. Focus on important tasks and chores; we will take care of your water routine. 
	You can subscribe to three plans, each with free delivery but with different conditions and perks. With Basic plan, you get 12 coupons and 3 deposit bottles. As a perk, we give you a free water pump. </p>
								</div>
								<div class="detail">
									<p>Optimal plan provides 24 coupons and 3 deposit bottles. As a perk, choose between 5 additional deposit bottles or 3 deposit bottles + free water pump. With Corporate plan, you can enjoy the utmost customization, as our system will count how many bottles you need and how many times we deliver it, depending on the number of people present in the office. </p>
								</div>
							</div>
						</div>
						
						<div id="sprinkle-bett">
							<header>
								<h3><span>Sprinkle PET Bottles: <br>6 L, 1.5 L, 550 ml, 350 ml</span> </h3>
								<nav>
									<ul class="d-flex center-xs around-sm">
										<li><i><img src="assets/imgs/nav-sm-1.png"></i><small>Going out</small></li>
										<li><i><img src="assets/imgs/nav-sm-2.png"></i><small>Home use</small></li>
										<li><i><img src="assets/imgs/nav-sm-3.png"></i><small>Dining</small></li>
										<li><i><img src="assets/imgs/nav-sm-4.png"></i><small>Events & Gatherings</small></li>
										<li><i><img src="assets/imgs/nav-sm-5.png"></i><small>Camping</small></li>
									</ul>
								</nav>
							</header>

							<div class="row _chd-cl-xs-12-xsh-06 between-sm pt30-xs mb30-xs">
								<div class="detail">
									<p>From 350 ml to 6 liters - that’s the range to choose from when you need portability. This is a great solution if you need many bottles for banquets and training sessions, for picnics and school events. The bottles are provided in packages. You can subscribe to the delivery of packages or make a one-time order. </p>
								</div>
								<div class="detail">
									<p>What you’ll see once you unpack the bottles is their eye-catching design. We are not the only ones who think so, as we got Red Dot Award 2014, Good Design Award 2013, and DEmark Design Excellent Award 2013. However, the most important thing is that our bottles are safe and made according to international standards, such as HACCP and GMP.</p>
								</div>
							</div>
						</div>
						
						<div id="accessories">
							<header>
								<h3><span>Coolers and accessories</span></h3>
							</header>

							<div class="row _chd-cl-xs-12-xsh-06 between-sm pt30-sm mb30-xs">
								<div class="detail">
									<p>Coolers provide clean water anywhere the standard electrical outlet is available. Some coolers can be a source of cold refreshing water only, some will heat it up for you. Here we give you an array of choices in terms of design, functionality, and even add-ons (like the little fridge at the bottom).</div>
								<div class="detail">
									<p>You can purchase them as a one-time order or you can additionally subscribe to 18.9 liters water delivery. Accessories include water pumps, which you can buy separately or get for free as a part of Basic or Optimal plan. </p>
								</div>
							</div>
						</div>
					</div>
					<!-- p3 -->
					<div class="page-bx" id="p3">
						<h2 class="h-topic mb20-xs">Advanced Technology for The Ultimate Purity</h2>

						<div class="row _chd-cl-xs-12-xsh-06 between-sm mb30-xs">
							<div class="detail">
								<figure class="mb10-xs">
									<a href="assets/imgs/cor-quality-c1.jpg" data-fancybox="images"><img src="assets/imgs/cor-quality-c1.jpg"><i class="ic-vdo"></i></a>
								</figure>
								<p>When it comes to hygiene, more is better! Before delivering to your hands and nourishing your body, the water is taken from a natural resource, gone through the advanced technology of purification and disinfection systems. Random quality control is done every 2 hours. Samples are constantly submitted to the official authority for verifications.</p>
							</div>
							<div class="detail">
								<figure  class="thm-vdo mb10-xs">
									<a href="assets/imgs/cor-quality-c2.jpg" data-fancybox="images"><img src="assets/imgs/cor-quality-c2.jpg"></a>
								</figure>
								<p>All our products are designed to be premium quality drinking water, with world-class quality control from APHA, AWA, WEF and quality certificates from many institutions such as HACCP, ISO 9001:2008, GMP.</p>
							</div>
						</div>
						
						<figure class="mb30-xs">
							<a href="assets/imgs/cor-quality-c3.jpg" data-fancybox="images"><img src="assets/imgs/cor-quality-c3.jpg"></a>
						</figure>
						<ul class="step">
							<li>
								<b>Step 1</b>
								<p>Ultrafiltration uses a hollow fiber membrane to effectively remove particles of 0.01 micron such as turbidity, suspended particles, microplastics, silica, bacteria, yeasts, fungi, viruses, and parasites.</p>
							</li>
							<li>
								<b>Step 2</b>
								<p>Reverse Osmosis eliminates contaminants as small as 0.0001 micron and filters out saltiness, bacteria, viruses, hard metals, as well as any toxic compounds. The company establishes a continuous quality control system to maintain the highest standard.</p>
							</li>
							<li>
								<b>Step 3</b>
								<p>Ultraviolet light to disinfect micro-organisms.</p>
							</li>
							<li>
								<b>Step 4</b>
								<p>Ozonation to complete micro-organism disinfection.</p>
							</li>
						</ul>
						<p class="mb30-xs">Also, every 2 hours random inspections are carried out on all filtration processes to ensure absolute cleanliness.</p>
						
						<h2 class="h-topic mb10-xs">Let the quality and awards tell of our proficiency</h2>
						<div class="row _chd-cl-xs-12-xsh-06 between-sm pt30-sm mb30-xs">
							<span><img src="assets/imgs/cor-rewards1.png"></span>
							<span><img src="assets/imgs/cor-rewards2.png"></span>
						</div>
						
						<div class="row _chd-cl-xs-12-xsh-06 between-sm pt10-sm mb30-xs">
							<div class="detail">
								<p>If you want to talk about global guarantee quality, APHA, AWWA, WEF are the associations who have certified us. If you want to talk about standards that drinking water companies should have, HACCP, ISO 9001: 2008, GMP are certificates that we are proud to put on the side of the office. </div>
							<div class="detail">
								<p>We also won the prizes that other drinking water brands never had; Red Dot Award 2014: Best of the best packaging design, iF Design Award 2014, Good Design Award 2013, DEmark Design Excellent Award. What this shows is that we design the quality of both drinking water and packaging to make Sprinkle the best drinking water in your hands.</p>
							</div>
						</div>
						
						<figure class="mb30-xs">
							<a href="assets/imgs/cor-quality-c4.jpg" data-fancybox="images"><img src="assets/imgs/cor-quality-c4.jpg"></a>
						</figure>
						<h2 class="h-topic mb10-xs">Contaminant-free Bottle for The Ultimate Pure Water</h2>
						<div class="row _chd-cl-xs-12-xsh-06 between-sm pt30-sm mb30-xs">
							<div class="detail">
								<p>Not only focusing on the purity of water, bottle hygiene and safety are also our top priority. Sprinkle water bottle is made from the clear-blue polycarbonate, the clearest and most durable plastic with the ability to withstand up to 140-degree Celsius heat.</div>
							<div class="detail">
								<p>It is the same kind of plastic that is used for baby bottles. Every gallon bottle is exposed to a five-step high-standard washing procedure before it can be refilled.</p>
							</div>
						</div>
						
						<h2 class="h-topic mb10-xs">5 Stages of Standard Bottle Disinfection Process</h2>
						<ol>
							<li>Ultrafiltration uses a hollow fiber membrane to effectively remove particles of 0.01 micron such as turbidity, suspended particles, microplastics, silica, bacteria, yeasts, fungi, viruses, and parasites.</li>
							<li>Reverse Osmosis eliminates contaminants as small as 0.0001 micron and filters out saltiness, bacteria, viruses, hard metals, as well as any toxic compounds. The company establishes a continuous quality control system to maintain the highest standard.</li>
							<li>Ultraviolet light to disinfect micro-organisms.</li>
							<li>Ozonation to complete micro-organism disinfection.</li>
							<li>Rinse with water of the same quality as the bottled water. Quality check with a random Swab Test. Fill up the bottle with drinking water and seal with our automated machinery to ensure the ultimate purity and safety.</li>
						</ol>
						<p class="mb30-xs">Then the empty bottle is filled in with drinking water and sealed with a bottle cap by automatic machinery for maximum safety and cleanliness.</p>
						
						<div class="row _chd-cl-xs-12-xsh-06 between-sm mb30-xs">
							<figure class="mb30-xs">
								<a href="assets/imgs/cor-quality-c5.jpg" data-fancybox="images"><img src="assets/imgs/cor-quality-c5.jpg"></a>
							</figure>
							<div class="detail">
								<h3 class="h-topic mb10-xs">Single-use Cap. Secure and Safe</h3>
								<p>Every Smart bottle cap of 18.9 liters of Sprinkle water is designed for single use. Screw caps are not allowed to reuse. A bottle cap is stamped with a clear logo alongside specifying the date of manufacture and expiration date with the international production standards that can be trusted for cleanliness and safety. </p>
							</div>
						</div>
						
					</div>
					<!-- p4 -->
					<div class="page-bx" id="p4">
						<h2 class="h-topic mb20-xs">Adapted to Global Changes. Perfect for Modern Consumer</h2>
						<figure class="mb30-xs">
							<a href="assets/imgs/cor-delivery-c1.jpg" data-fancybox="images"><img src="assets/imgs/cor-delivery-c1.jpg"></a>
						</figure>
						
						<div class="row _chd-cl-xs-12-xsh-06 between-sm pt30-sm mb30-xs">
							<div class="detail">
								<p>Time is precious in this modern world. With our ordering system, Sprinkle Web App is here to make your drinking water purchase easier in just a few clicks. 16 of our Sprinkle drinking water distribution centers manage the delivery with the utmost efficiency, covering all 16 areas in Bangkok and the Metropolitan region.</p>
							</div>
							<div class="detail">
								<p>No extra delivery charges. Home, office, building, condominium, apartment, or factory – we are always ready to deliver the refreshing drinking water at your door.</p>
							</div>
						</div>
						
						<figure class="mb30-xs">
							<a href="assets/imgs/cor-delivery-c2.jpg" data-fancybox="images"><img src="assets/imgs/cor-delivery-c2.jpg"></a>
						</figure>
					</div>

		</div>
	</section>
	
		
</div>

<?php include("incs/footer.html"); ?>

<!-- Java Script -->
<?php include("incs/js.html"); ?>
<script type="text/javascript" src="assets/js/jquery.idTabs.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	//expand step2
	$('.cor-page-tabs .idTabs>li a').click(function(){
		$('.cor-page-tabs .idTabs>li').removeClass('active');
		$(this).parent().toggleClass('active');
	});
});
</script>
<!-- /JS -->
</body>
</html>
