<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html"); ?>
<!-- /Top Head -->
<script>
  //<![CDATA[
  $(document).ready(function(){
      $('#navigation>ul>li>a').removeClass('selected');
	  $('#navigation>ul>li:nth-child(5)>a').addClass('selected');
  });
  //]]>
</script>
<body class="page-detail">
<!-- header -->
<?php include("incs/header.html"); ?>
<!-- /header -->
<div id="toc" class="warpper">
	
	
	<section id="home" class="sec-corporrate">
		<div class="container">
			
			<h2 class="h-topic">Corporrate</h2>

		</div>
	</section>
	
		
</div>

<?php include("incs/footer.html"); ?>

<!-- Java Script -->
<?php include("incs/js.html"); ?>
<!-- /JS -->
</body>
</html>
