<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html"); ?>
<!-- /Top Head -->
<script>
  //<![CDATA[
  $(document).ready(function(){
      $('#navigation>ul>li>a').removeClass('selected');
	  $('#navigation>ul>li:nth-child(2)>a').addClass('selected');
  });
  //]]>
</script>
<body class="page-checkout">
<!-- header -->
<?php include("incs/header.html"); ?>
<!-- /header -->
<div id="toc" class="wrapper">
	
	
	<section id="home" class="sec-pd sec-checkout">
		<div class="full">

			<form method="post" action="thankyou-en.php" class="row _chd-cl-xs-12-sm-06 main-cko">
				<!-- Cart -->
				<div id="CartCheckout" class="cart-checkout dropdown-content">
					<h2 class="title">Checkout : </h2>				
					<?php /*?><div class="total-mob">
						<b>Total:</b>
						<div class="price">
							<big class="total_price">฿13,145.87</big>
							<small>Includinng VAT 7%</small>
						</div>
					</div><?php */?>
					<ul class="cart-list">
						<?php for($i=1; $i<=8; $i++) { 
				
				  $img_url = "assets/products/deposit.png";
				  $name = "Sprinkle Water";
				  $price = "0.-";
				  switch($i)
				  {
					  case "1": 
								  $img_url = "assets/products/350ml.png";
								  $name = "Sprinkle 350ml.";
								  $price = "฿75.-";
						  break;
					  case "2": 
								  $img_url = "assets/products/550ml.png";
								  $name = "Sprinkle 550ml.";
								  $price = "฿70.-";
						  break;
					  case "3": 
								  $img_url = "assets/products/1.5L.png";
								  $name = "Sprinkle 1.5L.";
								  $price = "฿66.50.-";
						  break;
					  case "4": 
								  $img_url = "assets/products/6l.png";
								  $name = "Sprinkle 6L";
								  $price = "฿149.-";
						  break;
					  case "5": 
								  $img_url = "assets/products/18.9L-pro.png";
								  $name = "Promotion (x24 Coupons)";
								  $price = "฿2,160.-";
						  break;
					  case "6": 
								  $img_url = "assets/products/18.9L-pro.png";
								  $name = "Promotion (x12 Coupons)";
								  $price = "฿1,404.-";
						  break;
					  case "7": 
								  $img_url = "assets/products/coupons24.png";
								  $name = "Sprinkle Bottled Water \n Coupon x24";
								  $price = "฿1,560.-";
						  break;
					  case "8": 
								  $img_url = "assets/products/coupons12.png";
								  $name = "Sprinkle Bottled Water \n Coupon x12";
								  $price = "฿804.-";
						  break; 
					  case "9": 
								  $img_url = "assets/products/deposit.png";
								  $name = "Deposit Bottle";
								  $price = "฿200.-";
						  break;
					  case "10": 
								  $img_url = "assets/products/auto-water-pump.png";
								  $name = "Automatic Water Pump";
								  $price = "฿270.-";
						  break;
					  case "11": 
								  $img_url = "assets/products/Smart-Dispenser.png";
								  $name = "Touch Smart Electric \n Water Dispenser";
								  $price = "฿360.-";
						  break;
					  case "12": 
								  $img_url = "assets/products/BL724.png";
								  $name = "BL 724";
								  $price = "฿9,800.-";
						  break;
					  case "13": 
								  $img_url = "assets/products/HC335.png";
								  $name = "HC 335";
								  $price = "฿5,800.-";
						  break;
					  case "14": 
								  $img_url = "assets/products/TSCO170.png";
								  $name = "TSHC170";
								  $price = "฿5,550.-";
						  break;
					  case "15": 
								  $img_url = "assets/products/TSHC170.png";
								  $name = "TSCO170";
								  $price = "฿5,150.-";
						  break;
				  		}

						 ?>
						<li>
							<div class="pd">
								<div class="wrapinfo">
									<figure>
										<img src="<?php echo $img_url; ?>" alt="<?php echo $name; ?>">
									</figure>
									<div class="info">
										<h3 class="title">
											<?php echo nl2br($name); ?>
										</h3>
										<p class="price-average">18 bottles/pack <a class="t-primary" data-fancybox="" data-src="#pd-d0<?php echo $i; ?>" href="javascript:;">(details)</a></p>
									</div>
								</div>
								<div class="wrap-item d-flex end-xs txt-l">
									<div class="box__number mr10-xs hidden-xs hidden-sm hidden-md">
											<a class="decrease">-</a>
											<input type="text" class="number" readonly="readonly" name="counter-pd<?php echo $i; ?>" id="counter-pd<?php echo $i; ?>" value="1">
											<a class="increase">+</a>
									</div>
									<div class="sum-item">
										<div class="cv-select visible-xs visible-sm visible-md"><select>
											<option value="1">1 item</option>
											<option value="2">2 items</option>
											<option value="3">3 items</option>
											<option value="4">4 items</option>
											<option value="5">5 items</option>
										</select></div>

										<div class="price-total d-flex flex-column end-xs">
											<big><?php echo $price; ?></big>
											<small class="hidden-xs hidden-sm hidden-md">Excluding VAT 7%</small>
										</div>
									</div>
								</div>
								
								<!-- popup detail -->
								<?php if($i==5) { ?>
								<div id="pd-d0<?php echo($i) ?>" class="ft-detail" style="display: none">
									<div class="head-title d-flex start-xs">
											<figure>
												<img src="assets/products/18.9L-pro.png" alt="18.9L">
											</figure>
											<div class="pd-name">
												<h3 class="title">
													Promotion (x24 Coupons)
												</h3>
												<p class="price-average">64 baht/bottle</p>
											</div>
										</div>

										<div class="type-purchase">
											<p>Choose free promotion: </p>
											<div class="d-flex flex-column">
												<div class="mz-chk mb10-xs">
													<input name="promotion1" id="promotion1-1" value="0" type="radio" checked>
													<label for="promotion1-1">5 coupons</label>
												</div>
												<div class="mz-chk">
													<input name="promotion1" id="promotion1-2" value="1" type="radio">
													<label for="promotion1-2">3 coupons + Automatic water pump</label>
												</div>
											</div>
										</div>
										<div class="text-detail">
											bottle deposit of 3 bottles 200 Baht/bottle <del class="t-gray">(300 Baht/bottle)</del>
										</div>
										<div class="total-mob">
											<b>Total:</b>
											<div class="price">
												<big class="total_price">฿2,160.-</big>
												<small>Includinng VAT 7%</small>
											</div>
										</div>
										<div class="ctrl-btn">
											<button type="button" class="ui-btn-primary btn-block btn-addcart" data-fancybox-close="">Add to cart</button>
										</div>
								</div>
								<?php } elseif($i==6) {?>
								<div id="pd-d0<?php echo($i) ?>" class="ft-detail" style="display: none">
									<div class="head-title d-flex start-xs">
											<figure>
												<img src="assets/products/18.9L-pro.png" alt="18.9L">
											</figure>
											<div class="pd-name">
												<h3 class="title">
													Promotion (x12 Coupons)
												</h3>
												<p class="price-average">64 baht/bottle</p>
											</div>
										</div>

										<div class="type-purchase">
											<p>Choose free promotion: </p>
											<div class="d-flex flex-column">
												<div class="mz-chk mb10-xs">
													<input name="promotion2" id="promotion2" value="0" type="radio" checked>
													<label for="promotion2">Touch Smart electric water dispenser</label>
												</div>

											</div>
										</div>
										<div class="text-detail">
											bottle deposit of 3 bottles 200 Baht/bottle <del class="t-gray">(300 Baht/bottle)</del>
										</div>
										<div class="total-mob">
											<b>Total:</b>
											<div class="price">
												<big class="total_price">฿1,404.-</big>
												<small>Includinng VAT 7%</small>
											</div>
										</div>
										<div class="ctrl-btn">
											<button type="button" class="ui-btn-primary btn-block btn-addcart" data-fancybox-close="">Add to cart</button>
										</div>
								</div>
								<?php } else {?>
								<div id="pd-d0<?php echo($i) ?>" class="ft-detail" style="display: none">
									<div class="head-title d-flex start-xs">
										<figure>
											<img src="<?php echo $img_url; ?>" alt="<?php echo $name; ?>">
										</figure>
										<div class="pd-name">
											<h3 class="title">
												<?php echo nl2br($name); ?>
											</h3>
											<p class="price-average">18 bottles/pack</p>
										</div>
									</div>
									<div class="quantity d-flex between-xs">
										<div class="l">
											<p><b>Quantity:</b> (packs)</p>
											<span>- 18 bottles</span>
										</div>
										<div class="r pd">
											<div class="box__number">
													<a class="decrease">-</a>
													<input type="text" class="number" readonly="readonly" name="sub-counter-pd<?php echo($i) ?>" id="sub-counter-pd<?php echo($i) ?>" value="0">
													<a class="increase">+</a>
											</div>
										</div>
									</div>
									<div class="type-purchase">
										<p>Choose type of purchase:</p>
										<div class="d-flex pt5-xs">
											<div class="mz-chk mr20-xs">
												<input name="dv-purchase-<?php echo($i) ?>" id="d-onetime-<?php echo($i) ?>" value="0" type="radio" checked>
												<label for="d-onetime-<?php echo($i) ?>">One time order</label>
											</div>
											<div class="mz-chk">
												<input name="dv-purchase-<?php echo($i) ?>" id="d-subscribtion-<?php echo($i) ?>" value="1" type="radio">
												<label for="d-subscribtion-<?php echo($i) ?>">Subscribtion</label>
											</div>
										</div>
									</div>
									<div class="text-detail">
										Delivery cost 5 Baht/pack. Minimum order 2 packs. Order over 20 packs at 70 Baht/pack
									</div>
									<div class="total-mob">
										<b>Total:</b>
										<div class="price">
											<big class="total_price">฿3,145.87</big>
											<small>Includinng VAT 7%</small>
										</div>
									</div>
									<div class="ctrl-btn">
										<button type="button" class="ui-btn-primary btn-block btn-addcart" data-fancybox-close="">Add to cart</button>
									</div>
								</div>
								<?php } ?>
								<!-- /popup detail -->
							</div>
							<div class="btn-del visible-md visible-sm visible-xs" onclick="$(this).parents('li').remove();"></div>
						</li>
						<?php } ?>
					</ul>
					<div class="total-bar">
						<div class="pd-items">
							<big class="items">8</big>
							<div>
								Product items
								<b>in the order</b>
							</div>
						</div>
						<div class="price">	
							<big class="total_price">฿6,145.87</big>
							<small>Includinng VAT 7%</small>
						</div>
					</div>
					<div class="sw-cart-mob"><img src="assets/imgs/ic-angle-top-chkout.png"></div>

				</div>
				<!-- /Cart -->
				<!-- input form -->
				<div class="wrap-form">
					<ul class="idTabs d-flex center-xs">
						<li><a class="selected" href="#member" title="Sprinkle Member">Sprinkle Member</a></li>
						<li><a href="#register" title="New profile">New profile</a></li>
					</ul>
					<div class="contentTabs">
						<!-- Member -->
						<div id="member" class="form-tab">

							<div class="form-contact">
								<fieldset class="f-info">
									<legend class="hid">Member</legend>
									<ul class="tab-type d-flex center-xs _chd-mh10 mb20-xs mb30-sm">
										<li class="mz-chk">
											<input name="member-type" id="t-mwater" value="0" type="radio">
											<label for="t-mwater">18.9 L.</label>
	
										</li>
										<li class="mz-chk">
											<input name="member-type" id="t-bett" value="1" type="radio">
											<label for="t-bett">Small Bottles</label>
										</li>
									</ul>

									<div class="data-main row _chd-cl-xs-12-xsh-06">
										
										<div class="rw">
											<label for="cu_tel">Mobile No.<span class="require">*</span></label>
											<input type="tel" id="cu_tel" name="cu_tel" class="txt-box" placeholder="e.g. 102 01 23 004" maxlength="10" onBlur="openpopup()">
											
											<a class="hid" id="call-popup-mobile" data-fancybox data-src="#popup-chk-mobile" href="javascript:;">check mobile!</a>
											<a class="hid" id="call-popup-contact" data-fancybox data-src="#popup-contact" href="javascript:;">Not found!. Please contact now.</a>
											<!-- popup check mobile -->
											<div style="display: none;" class="thm-popup mid txt-c" id="popup-chk-mobile">
												<div class="inner">
													<!-- step 1 -->
													<div id="chk-step1" onClick="$(this).fadeOut(1).next().fadeIn(1);">
														<i class="icon d-flex center-xs">
															<img src="assets/imgs/icon-OTP-chk.png" height="60">
														</i>
														<h2 class="title">Checking mobile no.: +66 81 234 56 78</h2>
														<!--<p class="desc">You are awesome.</p>-->
														<div class="stage">
															<div class="dot-flashing"></div>
														</div>
													</div>
													<!-- step 2 -->
													<div id="chk-step2">
														<i class="icon d-flex center-xs">
															<img src="assets/imgs/icon-OTP.png" height="60">
														</i>
														<h2 class="title">Input OTP code from SMS message:</h2>
														<div class="rw mt10-xs">
															<input type="tel" class="txt-box _self-pa15 txt-c" id="OTP" name="OTP_code" maxlength="6">
														</div>
														<div class="ctrl-btn">
															<a href="javascript:;" class="ui-btn-primary btn-block" onClick="$(this).parents('#chk-step2').fadeOut(1).next().fadeIn(1);">CONFIRM</a>
														</div>
													</div>
													<!-- step 3 -->
													<div id="chk-step3">
														<i class="icon d-flex center-xs" onClick="$(this).parents('#chk-step3').fadeOut(1).next().fadeIn(1);">
															<img src="assets/imgs/icon-OTP.png" height="60">
														</i>
														<h2 class="title" onClick="$(this).parents('#chk-step3').fadeOut(1).next().fadeIn(1);">Input OTP code from SMS message:</h2>
														<div class="stage">
															<div class="dot-flashing"></div>
														</div>
														<div class="ctrl-btn">
															<a href="javascript:;" class="ui-btn-primary btn-block disabled" onClick="$(this).parents('#chk-step3').fadeOut(1); $('#chk-step4').hide(); $('#chk-step-wrong').fadeIn(1);">CONFIRM</a>
														</div>
													</div>
													<!-- step 4 -->
													<div id="chk-step4">
														<i class="icon d-flex center-xs">
															<img src="assets/imgs/icon-OTP-check.png" height="60">
														</i>
														<h2 class="title">Your OTP code:</h2>
	
														<div class="msg pb10-xs">
															<div class="big t-blue"><big>SUCCEEDED</big></div>
														</div>
														<div class="ctrl-btn">
															<a href="javascript:;" class="ui-btn-primary btn-block" data-fancybox-close="" onClick="$('#member .form-contact').children('fieldset').addClass('show');">CONTINUE</a>
														</div>
													</div>
													<!-- step wrong -->
													<div id="chk-step-wrong">
														<div class="help txt-c">
															<b>Have an issue? </b>
															<p><small class="d-block">Please contact our call center </small><a href="tel:027127272">02-712-7272</a> or LINE ID <a href="http://line.me/ti/p/~@sprinklewater">@sprinklewater</a></p>
														</div>
														<i class="icon d-flex center-xs">
															<img src="assets/imgs/icon-OTP.png" height="60">
														</i>
														<h2 class="title">Input OTP code from SMS message:</h2>
	
														<div class="rw mt10-xs">
															<input type="tel" class="txt-box _self-pa15 txt-c" id="OTP" name="OTP_code" maxlength="6">
															<p class="txt-c t-red mt10-xs">Wrong code</p>
														</div>
														<div class="ctrl-btn row _chd-cl-xs-06">
															<span><a href="javascript:;" class="ui-btn-border btn-block" onClick="$(this).parents('#chk-step-wrong').fadeOut(1); $('#chk-step-again').fadeIn(1);">SEND AGAIN</a></span>
															<span><a href="javascript:;" class="ui-btn-primary btn-block" onClick="$(this).parents('#chk-step-wrong').fadeOut(1); $('#chk-step4').fadeIn(1);">CONFIRM</a></span>
														</div>
													</div>
													
													<!-- step again -->
													<div id="chk-step-again">
														<div class="help txt-c">
															<b>Have an issue? </b>
															<p><small class="d-block">Please contact our call center </small><a href="tel:027127272">02-712-7272</a> or LINE ID <a href="http://line.me/ti/p/~@sprinklewater">@sprinklewater</a></p>
														</div>
														<i class="icon d-flex center-xs">
															<img src="assets/imgs/icon-OTP.png" height="60">
														</i>
														<h2 class="title">Input OTP code from SMS message:</h2>
	
														<div class="rw mt10-xs">
															<input type="tel" class="txt-box _self-pa15 txt-c" id="OTP" name="OTP_code" maxlength="6">
															<p class="txt-c t-blue mt10-xs"><a href="javacsript:;">New code have been sent!</a></p>
														</div>
														<div class="ctrl-btn row _chd-cl-xs-06">
															<span><a href="javascript:;" class="ui-btn-border btn-block" onClick="$(this).parents('#chk-step-again').fadeOut(1); $('#chk-step-again').fadeIn(1);">Next Try In 0:59</a></span>
															<span><a href="javascript:;" class="ui-btn-primary btn-block" onClick="$(this).parents('#chk-step-again').fadeOut(1); $('#chk-step4').fadeIn(1);">CONFIRM</a></span>
														</div>
													</div>
													
													
												</div>
											</div>
											<!-- /popup check mobile -->
											<!-- popup contactnow -->
											<div style="display: none;" class="thm-popup mid txt-c" id="popup-contact">
												<div class="inner">
													<div id="chk-step-contact">
														<div class="help txt-c">
															<p>We didn’t find your mobile number in the system.</p>
															<p><small class="d-block">Please contact our call center </small><a href="tel:027127272">02-712-7272</a> or LINE ID <a href="http://line.me/ti/p/~@sprinklewater">@sprinklewater</a></p>
														</div>
														<div class="ctrl-btn">
															<a href="tel:027127272" class="ui-btn-primary btn-block" data-fancybox-close="">CONTACT NOW</a>
														</div>
													</div>
													
												</div>
											</div>
											
											<script type="text/javascript">
												function openpopup() {
													var mobile = document.getElementById('cu_tel').value;
													if(mobile.length < 10) {
														$('#call-popup-contact').trigger('click');
													} else {
														$('#call-popup-mobile').trigger('click');
													}
													//$('#member .form-contact').children('fieldset').addClass('show');
												}
											</script>
											<!-- /popup contactnow -->
										</div>
										<div class="rw chk">
											<label for="member_name">Contact Name<span class="require">*</span></label>
											<input type="text" id="member_name" name="member_name" class="txt-box">
										</div>
										<div class="rw chk">
											<label for="cu_email">E-mail<span class="require">*</span></label>
											<input type="tel" id="cu_email" name="cu_email" class="txt-box">
										</div>
										<div class="rw chk">
											<label for="member_code">Member ID<span class="require">*</span> <a class="t-blue" href="#">(Forgot your Member ID)</a></label>
											<!--<input type="text" id="member_code" name="member_code" class="txt-box">-->
											<select id="topic" name="topic" class="select2" data-placeholder="Select Member ID">
											  <option></option>
											  <option value="1">0123456789 (ajksdjjskajdhksjahdkj...)</option>
												<option value="2">23049023940234 - (adress)</option>
												<option value="3">23049023940234 - (adress)</option>

											</select>
										</div>
									</div>

								</fieldset>		
								
								<fieldset class="f-dilivery-bett">
									<legend class="chk">Small Bottle Delivery Schedule</legend>
									<div class="row _chd-cl-xs-12">
									
										<div class="rw chk _self-cl-xs-12-sm-12">
											<label for="latlong">Delivery location</label>
											<div class="area-map">
												<input id="map-search" class="map-search" type="text" placeholder="ค้นหาสถานที่">
												<button title="ใช้ตำแหน่งปัจจุบัน" id="current-location" class="current-location" type="button" onclick="getCurrent()"><label></label></button>

											<!-- Map -->
												<div id="map_canvas" style=" width: 100%; height: 400px"></div>
												<div class="row rw _chd-cl-xs-06 _self-mb0">
													<div><input name="lat_value-mem" type="text" id="lat_value-mem" class="hid txt-box disabled" readonly /></div>
													<div><input name="lon_value-mem" type="text" id="lon_value-mem"  class="hid txt-box disabled" readonly /></div> 
												</div>
											<!-- End Map -->
																							
											</div>
										</div>
										<div class="rw chk">
											<label for="sansiri_delivery_day-mem">Delivery on</label>
											<input type="text" id="sansiri_delivery_day-mem" name="sansiri_delivery_day-mem" class="txt-box t-blue ic-calendar " readonly><!-- class="disabled" -->
										</div>
										<div class="rw chk">
											<label for="sansiri_delivery_location-mem">Deliver to</label>
											<div class="switch-radio _chd-mb05">
												<div class="mz-chk">
													<input name="sansiri_delivery_location-mem" id="switch_1-mem" value="Leave at Reception" type="radio">
													<label for="switch_1-mem">Leave at Reception</label>
												</div>
												<div class="mz-chk">
													<input name="sansiri_delivery_location-mem" id="switch_2-mem" value="Your room" type="radio">
													<label for="switch_2-mem">Your room</label>
												</div>
												<div class="mz-chk">
													<input name="sansiri_delivery_location-mem" id="switch_4-mem" value="Leave at gate" type="radio">
													<label for="switch_4-mem">Leave at gate</label>
												</div>
												<div class="mz-chk">
													<input name="sansiri_delivery_location-mem" id="switch_5-mem" value="Sprinkle call before making Delivery" type="radio">
													<label for="switch_5-mem">Sprinkle call before making Delivery</label>
												</div>
											</div>
										</div>
									</div>
								</fieldset>

								<fieldset class="f-dilivery">
									<legend class="chk">18.9 L Delivery Schedule:</legend>
									<div class="row _chd-cl-xs-12">
										<div class="rw chk">
											<label for="sansiri_delivery_day-mem">First delivery date</label>
											<input type="text" id="sansiri_delivery_day-mem" name="sansiri_delivery_day-mem" class="txt-box t-blue ic-calendar" readonly>
										</div>
										<div class="rw chk">
											<label for="sansiri_delivery_day-mem">Deliver every</label>
											<div class="d-flex pt5-xs">
												<div class="mz-chk mr20-xs">
													<input name="dv-cycle" id="d-saturday-mem" value="1" type="radio">
													<label for="d-saturday-mem">Saturday</label>
												</div>
												<div class="mz-chk">
													<input name="dv-cycle" id="d-thursday-mem" value="0" type="radio">
													<label for="d-thursday-mem">Thursday</label>
												</div>
											</div>
										</div>
									</div>
								</fieldset>
								
								<fieldset class="f-payment">
									<legend class="chk">Payment option</legend>
									<div class="option-payment bg-none chk">
										<ol>
											<li class="mz-chk mt10-xs">
												<input name="pay_channel-mem" id="pay-online-mem" value="0" type="radio">
												<label for="pay-online-mem">Pay online <img src="assets/imgs/ic-payment-online.png" alt="Scan Pay , VISA , Master" height="24"></label>
											</li>
											<li class="mz-chk mt10-xs">
												<input name="pay_channel-mem" id="pay-delivery-mem" value="1" type="radio">
												<label for="pay-delivery-mem">Pay on delivery</label>
											</li>
										</ol>
									</div>
								</fieldset>
								
								<div class="term mt15-xs chk">
									<small>By clicking "Confirm", you agree to our <a href="javascript:;">terms</a> and <a href="javascript:;">conditions</a> and <a href="javascript:;">privacy policy</a>.</small>
								</div>

								<div class="footer-submit _chd-cl-xs-06 center-xs middle-xs">
									<span><a href="products.php" class="ui-btn-border btn-block btn-lg">BACK</a></span>
									<span><input type="submit" id="submit-btn" class="ui-btn-primary btn-block btn-lg chk" value="CONFIRM"></span>
								</div>

								
							</div>
						</div>
						<!-- Register -->
						<div id="register" class="form-tab">
							<div class="form-contact">
								<fieldset class="f-info">
									<legend class="hid">Register</legend>
									<ul class="tab-type d-flex center-xs _chd-mh10 mb20-xs mb30-sm">
										<li class="mz-chk"><!--<a class="active" href="javascript:;" title="บุคคลธรรมดา" onClick="$('.tab-style li a').removeClass('active'); $(this).addClass('active'); $('.for-corp').fadeOut(100);">บุคคลธรรมดา</a>-->
											<input name="regis-type" id="t-personal" value="0" type="radio" onClick="$('.for-corp').fadeOut(100);">
											<label for="t-personal">Personal</label>
	
										</li>
										<li class="mz-chk"><!--<a href="javascript:;" title="นิติบุคคล" onClick="$('.tab-style li a').removeClass('active'); $(this).addClass('active');$('.for-corp').fadeIn(300);">นิติบุคคล</a>-->
											<input name="regis-type" id="t-corporate" value="1" type="radio" onClick="$('.for-corp').fadeIn(300);">
											<label for="t-corporate">Corporate</label>
										</li>
									</ul>
									<div class="data-main row _chd-cl-xs-12-xsh-06">
										<div class="rw">
											<label for="cu_name1">First name<span class="require">*</span></label>
											<input type="text" id="cu_name1" name="cu_name1" class="txt-box">
										</div>
										<div class="rw">
											<label for="cu_surname">Last name<span class="require">*</span></label>
											<input type="text" id="cu_surname" name="cu_surname" class="txt-box">
										</div>
										<div class="rw">
											<label for="cu_tel">Mobile No.<span class="require">*</span></label>
											<input type="tel" id="cu_tel" name="cu_tel" class="txt-box" placeholder="" maxlength="10">
										</div>
										<div class="rw">
											<label for="cu_email">E-mail<span class="require">*</span></label>
											<input type="email" id="cu_email" name="cu_email" class="txt-box">
										</div>
									</div>

									<div class="for-corp">
										<div class="row _chd-cl-xs-12">
											<div class="rw">
												<label for="cu_company">Corporate Name<span class="require">*</span></label>
												<input type="text" id="cu_company" name="cu_company" class="txt-box">
											</div>
											<div class="rw">
												<div class="box-radio pt10-sm mb10-xs pb10-xs pb0-sm _chd-cl-xs-12-pl0-pr0 middle-xs">
													<div class="mz-chk mb10-xs mb0-sm">
														<input name="is_head" id="t-master" value="1" type="radio">
														<label for="t-master">Head of Office</label>
													</div>

													<div class="mz-chk mr15-xs">
														<input name="is_head" id="t-brance" value="0" type="radio">
														<label for="t-brance">Branch line of Buisness</label>
													</div>
												</div>
												<input type="text" value="" name="branch_name" class="txt-box _self-cl-xs" placeholder="">
					
											</div>
											<div class="rw">
												<label for="id_card">Juristic Identification Number<span class="require">*</span></label>
												<input type="text" id="id_card" name="id_card" class="txt-box">
											</div>
											<div class="rw upload-attach row _chd-cl-xs-12-xsh-06">
												<label class="note">Please upload a copy of company registered document with tax number 
												<span class="t-gray">(File extension .jpg or .pdf 500 KB maximum 500kb)</span><span class="require">*</span>
												</label>
												
												<!--<button type="button" class="ui-btn-primary btn-block" data-fancybox data-src="#hidden-upload" onClick="$(this).fadeOut(100).next().removeClass('hid');">ATTACH FILE</button>
												<div id="hidden-upload" class="thm-popup mid" style="display: none; width: 400px;">-->
													<div class="file-upload-wrapper" data-text="*juristic Identification">
														<input type="file" class="file-upload-field" id="company_reg_file" name="company_reg_file" accept="image/*"><!--  open camera = capture-->
														<label class="hid" for="company_reg_file">ATTACH FILE</label>                                
													</div>
												<!--</div>-->

												<script>
												   document.getElementById("company_reg_file").onchange = function () {
													var reader = new FileReader();
													reader.onload = function (e) {
														document.getElementById("show_image_src").src = e.target.result;
													};
													reader.readAsDataURL(this.files[0]);
												};    
												/*input file*/
												$("form").on("change", ".file-upload-field", function(){ 
													$(this).parent(".file-upload-wrapper").attr("data-text", $(this).val().replace(/.*(\/|\\)/, '') ).addClass("added");
													$('.upload-attach .image').removeClass('hid');
												});
												/*clear image*/
												$(document).ready(function(){
													$('.image .close').click(function() {
														$(".file-upload-wrapper").attr("data-text", " " ).removeClass("added");
														$('#show_image_src').attr('src', 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==');
														$('.upload-attach .image').addClass('hid');
													});
												});
												</script>

												<figure class="_self-cl-xs-12-xsh-12 image hid">
													<img class="mb10-xs" id="show_image_src" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==" />
													<a class="close" href="javascript:;"></a>
												</figure>

												
											</div>
										</div>
									</div>

								</fieldset>

								<fieldset class="f-address">
									<legend>Delivery info:</legend>
									<div class="data-main row _chd-cl-xs-12-sm-06">
										<div class="rw">
											<label for="address">Address<span class="require">*</span></label>
											<input type="text" id="address" name="address" class="txt-box">
										</div>
										<div class="rw">
											<label for="village">Condomnium / Village:</label>
											<input type="text" id="village" name="village" class="txt-box">
										</div>
										<div class="rw _self-cl-xs-04-sm-04">
											<label for="building">Building</label>
											<input type="text" id="building" name="building" class="txt-box">
										</div>
										<div class="rw _self-cl-xs-04-sm-04">
											<label for="floor">Floor</label>
											<input type="text" id="floor" name="floor" class="txt-box">
										</div>
										<div class="rw _self-cl-xs-04-sm-04">
											<label for="room">Room</label>
											<input type="text" id="room" name="room" class="txt-box">
										</div>

										<div class="rw">
											<label for="soi">Soi</label>
											<input type="text" id="soi" name="soi" class="txt-box">
										</div>
										<div class="rw">
											<label for="road">Road<span class="require">*</span></label>
											<input type="text" id="road" name="road" class="txt-box">
										</div>
										<div class="rw">
											<label for="tambon">Subdistrict/Tambon<span class="require">*</span></label>
											<input type="text" id="tambon" name="tambon" class="txt-box">
										</div>
										<div class="rw">
											<label for="district">District/Amphoe<span class="require">*</span></label>
											<input type="text" id="district" name="district" class="txt-box">
										</div>

										<div class="rw">
											<label for="province">City<span class="require">*</span></label>
											<input type="text" id="province" name="province" class="txt-box">
										</div>
										<div class="rw">
											<label for="post_code">Post Code<span class="require">*</span></label>
											<input type="tel" id="post_code" name="post_code" class="txt-box">
										</div>

										<div class="rw _self-cl-xs-12-sm-12-mb0">
											<label for="landmark">Landmark / Remark / Note to Driver:</label>
											<input type="text" id="landmark" name="landmark" class="txt-box">
										</div>
										<div class="rw _self-cl-xs-12-sm-12">
											<label for="latlong">Delivery location<span class="require">*</span></label>
											<div class="area-map">
												<input id="map-search" class="map-search" type="text" placeholder="ค้นหาสถานที่">
												<button title="ใช้ตำแหน่งปัจจุบัน" id="current-location" class="current-location" type="button" onclick="getCurrent()"><label></label></button>

											<!-- Map2 -->
												<div id="map_canvas2" style=" width: 100%; height: 400px"></div>
												<div class="row rw _chd-cl-xs-06 _self-mb0">
													<div><input name="lat_value-mem" type="text" id="lat_value-mem" class="hid txt-box disabled" readonly /></div>
													<div><input name="lon_value-mem" type="text" id="lon_value-mem"  class="hid txt-box disabled" readonly /></div> 
												</div>
											<!-- End Map2 -->
											</div>
										</div>
									</div>
								</fieldset>		
								
								<fieldset class="f-dilivery-bett">
									<legend>Small Bottle Delivery Schedule</legend>
									<div class="row _chd-cl-xs-12">
										<div class="rw">
											<label for="sansiri_delivery_day">Delivery on</label>
											<input type="text" id="sansiri_delivery_day" name="sansiri_delivery_day" class="txt-box t-blue ic-calendar " readonly><!-- class="disabled" -->
										</div>
										<div class="rw">
											<label for="sansiri_delivery_location">Deliver to</label>
											<div class="switch-radio _chd-mb05">
												<div class="mz-chk">
													<input name="sansiri_delivery_location" id="switch_1" value="Leave at Reception" type="radio">
													<label for="switch_1">Leave at Reception</label>
												</div>
												<div class="mz-chk">
													<input name="sansiri_delivery_location" id="switch_2" value="Your room" type="radio">
													<label for="switch_2">Your room</label>
												</div>
												<div class="mz-chk">
													<input name="sansiri_delivery_location" id="switch_4" value="Leave at gate" type="radio">
													<label for="switch_4">Leave at gate</label>
												</div>
												<div class="mz-chk">
													<input name="sansiri_delivery_location" id="switch_5" value="Sprinkle call before making Delivery" type="radio">
													<label for="switch_5">Sprinkle call before making Delivery</label>
												</div>
											</div>
										</div>
									</div>
								</fieldset>

								<fieldset class="f-dilivery">
									<legend>18.9 L Delivery Schedule:</legend>
									<div class="row _chd-cl-xs-12-sm-06">
										<div class="rw">
											<label for="sansiri_delivery_day">First delivery date</label>
											<input type="text" id="sansiri_delivery_day" name="sansiri_delivery_day" class="txt-box t-blue ic-calendar" readonly>
										</div>
										<div class="rw">
											<label for="sansiri_delivery_day">Deliver every</label>
											<div class="d-flex pt5-xs">
												<div class="mz-chk mr20-xs">
													<input name="dv-cycle" id="d-saturday" value="1" type="radio">
													<label for="d-saturday">Saturday</label>
												</div>
												<div class="mz-chk">
													<input name="dv-cycle" id="d-thursday" value="0" type="radio">
													<label for="d-thursday">Thursday</label>
												</div>
											</div>
										</div>
									</div>
								</fieldset>
								
								<fieldset class="f-payment">
									<legend>Payment option</legend>
									<div class="option-payment bg-none">
										<ol>
											<li class="mz-chk mt10-xs">
												<input name="pay_channel" id="pay-online" value="0" type="radio">
												<label for="pay-online">Pay online <img src="assets/imgs/ic-payment-online.png" alt="Scan Pay , VISA , Master" height="24"></label>
												
											</li>
											<li class="mz-chk mt10-xs">
												<input name="pay_channel" id="pay-delivery" value="1" type="radio">
												<label for="pay-delivery">Pay on delivery</label>
												
											</li>
										</ol>
									</div>
								</fieldset>
								
								<div class="term mt15-xs">
									<small>By clicking "Confirm", you agree to our <a href="javascript:;">terms</a> and <a href="javascript:;">conditions</a> and <a href="javascript:;">privacy policy</a>.</small>
								</div>

								<div class="footer-submit _chd-cl-xs-06 center-xs middle-xs">
									<span><a href="products.php" class="ui-btn-border btn-block btn-lg">BACK</a></span>
									<span><input type="submit" id="submit-btn" class="ui-btn-primary btn-block btn-lg" value="CONFIRM"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /input form -->
			</form>

		</div>
	</section>
	
		
</div>

<?php /*?><?php include("incs/footer.html"); ?><?php */?>

<!-- Java Script -->
<?php include("incs/js.html"); ?>
<script type="text/javascript" src="assets/js/jquery.idTabs.min.js"></script>

<script src="https://line-sprinkle.beurguide.net/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(function(){
    $('#frmregister').validate({
        errorElement: 'div',
        errorClass: 'formerror',
        errorPlacement: function(error, element) {
			var name = element.attr('name');
            if (name == 'weekly_delivery_day') {
                element = $('#error_weekly_delivery_day');
            }
            error.insertAfter(element);
        }
    });
    /*$('.toggle_customer').click(function(){
        var customer_type = $(this).data('type');
        $('#customer_type').val(customer_type);
        $('.tab-style li a').removeClass('active');
        $(this).addClass('active');
        if (customer_type == 'person') {
            $('.for-corp').fadeOut(300);
        }
        else {
        else {
            $('.for-corp').fadeIn(300);
        }
    });
    $('#cu_companyupload').change(function(){
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#show_image_src').attr('src', e.target.result);
        };
        reader.readAsDataURL(this.files[0]);
    });
    $("form").on("change", ".file-upload-field", function(){ 
        $(this).parent(".file-upload-wrapper").attr("data-text", $(this).val().replace(/.*(\/|\\)/, '') );
        $('.upload-attach .image').removeClass('hid');
    });*/
});

var map;
var marker;
var my_latLng;
var timeId;
var imagepin;
function initMap() {
    imagepin = {
        url: "https://line-sprinkle.beurguide.net/assets/di/map-marker.png?v1",
        size: new google.maps.Size(48, 48),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(24, 48)
    };

    GGM = new Object(google.maps);
    my_latLng  = new GGM.LatLng(13.7331123,100.5817902);

	/*map = new GGM.Map(document.getElementById('map_canvas'), {
        center: my_latLng,
        zoom: 10,
        mapTypeControl: false
    });*/
	
	var myOptions = {
        zoom: 10,
        center: my_latLng,
        mapTypeControl: false
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

    map2 = new google.maps.Map(document.getElementById("map_canvas2"), myOptions);

    //$('<div/>').addClass('centerMarker').appendTo(map.getDiv());

    map.addListener('center_changed', function() {  
        my_latLng = this.getCenter();
        if (timeId) {
            clearTimeout(timeId);
        }
        timeId = setTimeout(addMarker, 300);
    });

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
                var pos = new GGM.LatLng(position.coords.latitude, position.coords.longitude);
                map.setCenter(pos);

                var currentLocation = document.getElementById('current-location');
                map.controls[google.maps.ControlPosition.RIGHT_TOP].push(currentLocation);
            }, function(error) {
                console.log('ERROR(' + error.code + '): ' + error.message);
            }
        );
    } else {
        addMarker();
        $('#current-location').hide();
    }

    var input = document.getElementById('map-search');
    var searchBox = new google.maps.places.SearchBox(input);

    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
    });

    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces(1);

        if (places.length == 0) {
            return;
        }

        places.forEach(function(place) {
            map.setCenter(place.geometry.location);

            my_latLng = place.geometry.location;
            addMarker();
        });
    });
}

function addMarker()
{
    $("#lat_value").val(my_latLng.lat());
    $("#lon_value").val(my_latLng.lng());

    if (marker) {
        marker.setMap(null);
    }
    marker = new google.maps.Marker({
        map: map,
        position: my_latLng,
        icon: imagepin
    });

    $.get('https://line-sprinkle.beurguide.net/index.php/th/mwater/getdds', { lat: my_latLng.lat(), lng: my_latLng.lng() }, function(data){
        $('#express_delivery_date').empty();
        for (var i=0; i<data.delivery_date.length; i++)
        {
            $('#express_delivery_date').append($("<option></option>").attr('value', data.delivery_date[i]).text(data.delivery_date[i]));
        }
        $('#show_weekly').empty();
        for (var key in data.delivery_day) {
            var div = $('<div class="mz-chk mr20-xs"></div>');
            div.html('<input type="radio" id="d-'+key+'" name="weekly_delivery_day" value="'+key+'" data-rule-required="true"><label for="d-'+key+'">'+data.delivery_day[key]+'</label>');
            $('#show_weekly').append(div);
            $('#d-'+key).rules('add', { required: true });
        }
    }, 'json');
}

function getCurrent() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
                var pos = new GGM.LatLng(position.coords.latitude, position.coords.longitude);
                map.setCenter(pos);
            }, function(error) {
                console.log('ERROR(' + error.code + '): ' + error.message);
            }
        );
    } else {
        // error current location
    }
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-1t2LR21g6rPXt7ATygwSx0w0UleNiAg&libraries=places&callback=initMap" async defer></script>
        <!-- End Map -->
<!-- /JS -->
</body>
</html>
